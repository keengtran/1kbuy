@extends('templates.admin.master')
@section('content')
<div class="breadcrumbs">
    <div class="breadcrumbs-inner">
        <div class="row m-0">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Trang chủ</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="#">Trang chủ</a></li>
                            <li><a href="#">Sản phẩm</a></li>
                            <li class="active">Cập nhật sản phẩm</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content">
    <div class="animated fadeIn">
        <div class="row">
            <!--/.col-->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><strong>Cập nhật sản phẩm</strong></div>
                    @include('errors.success')
                    <form action="{{ route('admin.product.update' , $product->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="card-body card-block">
                            <div class="form-group">
                              <label for="">Danh mục sản phẩm</label>
                              <select class="form-control px-18 py-0" name="category_id" id="">
                                  @foreach ($categories as $item)
                                    <option value="{{$item->id}}">{{ $item->name}}</option>
                                  @endforeach
                              </select>
                            </div>
                            <div class="form-group">
                                <label for="street" class=" form-control-label">Tên sản phẩm</label>
                                <input type="text" name="name" class="form-control" value="{{ $product->name}}">
                                @if ($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="description" class=" form-control-label">Mô tả</label>
                                <textarea name="description" class="form-control " id="editor1"> {!! $product->description!!}</textarea>
                                @if ($errors->has('description'))
                                    <span class="text-danger">{{ $errors->first('description') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="ship" class=" form-control-label">Ship</label>
                                <input type="text" name="ship" class="form-control" value="{{ $product->ship}}">
                                @if ($errors->has('ship'))
                                    <span class="text-danger">{{ $errors->first('ship') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="price" class=" form-control-label">Giá</label>
                                <input type="number" name="price" class="form-control" value="{{ $product->price}}">
                                @if ($errors->has('price'))
                                    <span class="text-danger">{{ $errors->first('price') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="promotion_price" class=" form-control-label">Giá khuyến mãi</label>
                                <input type="number" name="promotion_price" class="form-control" value="{{ $product->promotion_price}}">
                                @if ($errors->has('promotion_price'))
                                    <span class="text-danger">{{ $errors->first('promotion_price') }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                              <label for="">Ảnh sản phẩm</label>
                              <img src="{{ asset('upload/images/'. $product->image)}}" width="100px"  alt="">
                              <input type="file" class="form-control-file" name="image" id="">
                            </div>
                            <button type="submit" class="btn btn-primary">Cập nhật</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!-- .animated -->
</div>
<script>
    var options = {
      filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
      filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
      filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
      filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
    };
</script>
<script>
    CKEDITOR.replace('editor1', options);
</script>

<script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
<script>
$('textarea.editor1').ckeditor(options);
</script>
@endsection