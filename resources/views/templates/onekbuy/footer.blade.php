    <footer>
        <section class="footer-main" style="background-color: #ADD8E6;">
            <div class="container footer-custom">
                <div class="row footer-custom">
                    <div class="col-12 col-sm-6 col-xl-3">

                    <a href="{{route('onekbuy.index.index')}}"><img class="img-center" src="{{asset('./upload/images/'.$information->logo)}}" alt=""> </a>
                        <p style="font-size:120%">{!!$information->description!!}</p>
                    </div>
                    <div class="col-12 col-sm-6 col-xl-3">
                        <h4 style="color:#000055;font-size:140%;margin-bottom: 25px;"><i>NỀN TẢNG THANH TOÁN</i></h4>
                        <h5><i>Ví Điện Tử</i></h5>
                        <div>
                            <a href="{{route('onekbuy.faq.vidientu')}}"><img src="/asset/onekbuy/image/Capture.JPG" alt="" class="vi-dien"></a>
                            <a href="{{route('onekbuy.faq.vidientu')}}"><img src="/asset/onekbuy/image/Capture1.JPG" alt="" class="vi-dien"></a>
                            <a href="{{route('onekbuy.faq.vidientu')}}"><img src="/asset/onekbuy/image/Capture2.JPG" alt="" class="vi-dien"></a>
                        </div>
                        <div>
                            <a href="{{route('onekbuy.faq.vidientu')}}"><img src="/asset/onekbuy/image/Capture3.JPG" alt="" class="vi-dien"></a>
                            <a href="{{route('onekbuy.faq.vidientu')}}"><img src="/asset/onekbuy/image/Capture4.JPG" alt="" class="vi-dien"></a>
                            <a href="{{route('onekbuy.faq.vidientu')}}"><img src="/asset/onekbuy/image/Capture11.JPG" alt="" class="vi-dien"></a>
                        </div>

                        <h5><i>Tài Khoản Ngân Hàng</i></h5>
                        <div>
                            <a href="{{route('onekbuy.faq.vidientu')}}"><img src="/asset/onekbuy/image/Capture5.JPG" alt="" class="vi-dien"></a>
                            <a href="{{route('onekbuy.faq.vidientu')}}"><img src="/asset/onekbuy/image/Capture6.JPG" alt="" class="vi-dien"></a>
                            <a href="{{route('onekbuy.faq.vidientu')}}"><img src="/asset/onekbuy/image/Capture7.JPG" alt="" class="vi-dien"></a>
                        </div>
                        <div>
                            <a href="{{route('onekbuy.faq.vidientu')}}"><img src="/asset/onekbuy/image/Capture8.JPG" alt="" class="vi-dien"></a>
                            <a href="{{route('onekbuy.faq.vidientu')}}"><img src="/asset/onekbuy/image/Capture9.JPG" alt="" class="vi-dien"></a>
                            <a href="{{route('onekbuy.faq.vidientu')}}"><img src="/asset/onekbuy/image/Capture10.JPG" alt="" class="vi-dien"></a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-xl-3">
                        <h4 style="color:#000055;font-size:140%;margin-bottom: 25px;"><i>VỀ 1KBUY VIỆT NAM</i></h4>
                        <p style="font-size:120%"><strong>Địa chỉ : </strong>{{$information->address}}</p>
                        <p style="font-size:120%"><strong>Hotline : </strong>{{$information->phone}}</p>
                        <p style="font-size:120%"><strong>Email : </strong>{{$information->email}}</p>
                        <div class="container">
                            <img src="/asset/onekbuy/image/bo-cong-thuong-1.png" alt="" width="70%" style="margin-bottom: 15px">
                            <img src="/asset/onekbuy/image/bo-cong-thuong-2.png" alt="" width="70%">
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-xl-3">
                        <h4 style="font-size:140%;margin-bottom:25px;"><i>THÔNG TIN CẦN BIẾT</i></h4>
                        <h5 style="margin-bottom:20px;"><a href={{route('onekbuy.faq.cauhoi')}} target="_blank"><i class="hover-ifor" style="color:#000055">Câu hỏi thường gặp</i></a></h5>
                        <h5 style="margin-bottom:20px;"><a href={{route('onekbuy.faq.gioithieu')}} target="_blank"><i class="hover-ifor" style="color:#000055">Giới thiệu</i></a></h5>
                        <h5 style="margin-bottom:20px;"><a href={{route('onekbuy.faq.dieukhoan')}} target="_blank"><i class="hover-ifor" style="color:#000055">Điều khoản sử dụng</i></a></h5>
                        <h5 style="margin-bottom:20px;"><a href={{route('onekbuy.faq.chinhsach')}} target="_blank"><i class="hover-ifor" style="color:#000055">Chính sách bảo mật</i></a></h5>
                        <h5 style="margin-bottom:20px;"><a href={{route('onekbuy.faq.hoantien')}} target="_blank"><i class="hover-ifor" style="color:#000055">Chính Sách Hoàn Tiền 100%</i></a></h5>
                        <h5 style="margin-bottom:20px;"><a href={{route('onekbuy.faq.boithuong')}} target="_blank"><i class="hover-ifor" style="color:#000055">Chính Sách Sản Phẩm - Bồi Thường</i></a></h5>
                    </div>
                </div>
            </div>

        </section>
        <div class="footer-bottom" style="background-color: #E6E6FA;">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-2 d-flex justify-content-center">
                        <a href="https://www.facebook.com/01kbuy/"><p><i class="social fab fa-facebook-f"></i></p></a>
                        <a href="https://twitter.com/"><p><i class="social fab fa-twitter"></i></p></a>
                        <a href="https://www.instagram.com/"><p><i class="social fab fa-instagram"></i></p></a>
                    </div>
                    <div class="col-12 col-md-10 d-flex justify-content-end">
                            <p>Demo by <strong><a href="https://1kbuy.vn/">1kbuy</a></strong> Việt Nam <script type="text/javascript">document.write( new Date().getFullYear() );</script> © <strong><a href="https://1kbuy.vn/">1kbuy.vn</a></strong></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

@yield('js')
</body>
    
</html>