@extends('templates.onekbuy.master')
@section('content')
<div class="content">
    <div class="shop-background">
        <div class="container container-shop">
            <div class="row row-intro">
                <div class="col-12 col-lg-8 col-xl-9 intro-left ">
                    <div class="row-intro-left">
                        <div class="entry-content">

                            <p class="text-center has-huge-font-size"><strong><span style="color:#0d00a3"
                                        class="has-inline-color">Chính Sách Hoàn Tiền 100%</span></strong></p>



                            <p class="has-medium-font-size"><strong>Nội dung hỗ trợ hoàn tiền</strong></p>



                            <p style="font-size:18px">– Lệnh đặt sản phẩm và lệnh yêu cầu hoàn tiền tại 1KBUY.</p>



                            <p class="has-medium-font-size"><strong>Chính sách hoàn tiền</strong></p>



                            <p style="font-size:18px">Hoàn tiền 100% khi đặt lệnh sản phẩm trên hệ thống 1KBUY:</p>



                            <p style="font-size:18px">Chính sách hoàn tiền tại 1KBUY là nền tảng để hoạt động hình
                                thức kinh doanh của chúng tôi. Đồng thời giúp khách hàng yên tâm hơn để trải nghiệm
                                dịch vụ tại 1KBUY. Trên cơ sở tiết kiệm chi phí nhiều nhất cho khách hàng khi mua
                                sắm online các sản phẩm trên website 1kbuy.vn, chúng tôi cam kết hoàn tiền 100% cho
                                quý khách khi tham gia đặt lệnh sản phẩm trên hệ thống 1KBUY.</p>



                            <p style="font-size:18px">Tiền của khách hàng được hoàn lại khi số tiền sử dụng đặt lệnh
                                không thành công vượt quá 500.000 VND. Đạt đến giá trị hoàn tiền này, chúng tôi có
                                trách nhiệm giữ số tiền đó cho khách hàng đến khi khách hàng yêu cầu hoàn tiền. Các
                                lệnh đặt sau đó sẻ được tính cho vòng mua tiếp theo để đảm bảo khách hàng không bị
                                mất tiền. Và số tiền này phải là cấp số nhân của 500.000 VND. Ví dụ khách hàng đặt
                                999 lượt không thành công thì chỉ được hoàn tiền 500.000 VND khi có yêu cầu. Tất cả
                                số tiền còn lại sẻ được hoàn lại nếu khách hàng hủy dịch vụ của chúng tôi. </p>



                            <p style="font-size:18px">Lưu ý khi đặt lệnh mua hàng thành công số tiền khách hàng đã
                                sử dụng để đặt lệnh sản phẩm sẻ không được hoàn tiền. Số tiền này bằng có giá trị
                                bằng 30% giá trị sản phẩm đặt lệnh mua thành công. Ví dụ sản phẩm đặt mua thành công
                                có giá 1.500.000 VND và số tiền đặt lệnh là 1.000 VND thì 1.000 VND này không được
                                hoàn lại. Hoặc số tiền đặt là 499.000 VND thì 30% của 1.500.000 VND tướng ứng
                                450.000 VND sẻ không được hoàn tiền. Đồng nghĩa khách hàng có thể nhận bất kì sản
                                phẩm nào của 1KBUY với giá thấp nhất là 1.000 VND hoặc cao nhất là 30% giá sản phẩm
                                mà tối đa là 500.000 VND. Chúng tôi cũng không chấp nhận hoàn tiền trong trường hợp
                                khách hàng đặt lệnh mua thành công nhưng từ chối không mua hoặc không nhận sản phẩm.
                            </p>



                            <p style="font-size:18px">Khi đủ điều kiện hoàn tiền quý khách có thể tạo lệnh yêu cầu
                                hoàn tiền bất cứ khi nào. Tuy nhiên chúng tôi chỉ thực hiện việc hoàn tiền vào các
                                ngày 10, ngày 20, ngày 30 mỗi tháng. Quý khách hàng chú ý để không phải chờ đợi lâu
                                khi yêu cầu rút tiền quá xa các ngày này. Lệnh yêu cầu hoàn tiền được hướng dẫn tại
                                <a rel="noreferrer noopener" href="https://1kbuy.vn/cau-hoi-thuong-gap/"
                                    target="_blank">Hướng dẫn yêu cầu hoàn tiền</a> . Chúng tôi chỉ hoàn tiền khi
                                khách hàng gửi yêu cầu hoàn tiền.</p>



                            <p style="font-size:18px">Khi nhận được yêu cầu hoàn tiền từ quý khách chúng tôi sẻ kiểm
                                tra thông tin hệ thống sau đó gửi thông báo xác nhận hoàn tiền cho quý khách qua tin
                                nhắn MoMo. Khách hàng kiểm tra thông báo xác nhận và gửi lại phản hồi để nhận hoàn
                                tiền. Trong trường hợp quý khách hàng không phản hồi lại xác nhận hoàn tiền sau 48h
                                thì yêu cầu hoàn tiền bị hủy bỏ. Chúng tôi không giải quyết khiếu nại trong trường
                                hợp này. Khi đó nếu vẫn tiếp tục muốn nhận hoàn tiền, khách hàng phải đặt lệnh yêu
                                cầu hoàn tiền mới. Hoặc nếu thời gian phản hồi sau các ngày hoàn tiền thì sẻ được
                                hoàn tiền vào lần hoàn tiếp tiếp theo.</p>



                            <p style="font-size:18px">Không chấp nhận hoàn tiền khi khách hàng bị hủy tư cách thành
                                viên do vi phạm các điều khoản sử dụng tại 1KBUY hoặc khách hàng tự ý hủy sử dụng
                                dịch vụ của 1KBUY. </p>



                            <p class="has-medium-font-size"><strong>Phí hoàn tiền</strong></p>



                            <p style="font-size:18px">Tiền hoàn lại sẻ được chuyển đúng, đủ số tiền yêu cầu theo quy
                                định hoàn tiền qua tài khoản ví MoMo của quý khách và không thu phí hoàn tiền. Mỗi
                                lệnh yêu cầu sẻ được thực hiện hoàn tiền riêng biệt. Tiền hoàn lại quý khách có toàn
                                quyền sử dụng để tái đặt lệnh sản phẩm hoặc các mục đích cá nhân.</p>
                        </div>
                    </div>


                    <div style="clear:both;"></div>

                </div>
                @include('templates.onekbuy.post-bar')
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
        }
      
        function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        }
        $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
        })
      </script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
@endsection