@extends('templates.onekbuy.master')
@section('content')
<div class="content">
<div class="shop-background">
    <div class="container container-shop">
        <div class="row row-intro">
            <div class="col-12 col-lg-8 col-xl-9 intro-left ">
                <div class="row row-intro-left">
                    <div class="entry-content">

                        <p class="text-center has-huge-font-size"><strong><span style="color:#1700a3"
                                    class="has-inline-color">Giới Thiệu Về 1KBUY VIỆT NAM</span></strong></p>



                        <h1>Thông tin công ty</h1>



                        <p class="has-medium-font-size"><strong>Tên công ty:&nbsp;</strong>CÔNG TY TNHH 1KBUY
                            VIỆT NAM</p>



                        <p class="has-medium-font-size"><strong>Tên tiếng Anh:&nbsp;</strong>1KBUY VIETNAM
                            COMPNANY LIMITED</p>



                        <p class="has-medium-font-size"><strong>Tên viết tắt:&nbsp;</strong>1KBUY CO., LTD</p>



                        <p class="has-medium-font-size"><strong>Giấy phép kinh doanh số:&nbsp;</strong></p>



                        <p class="has-medium-font-size"><strong>Trụ sở chính:&nbsp;</strong>Tầng 1, Số 4 Nguyễn
                            Thị Minh Khai, Phường Đa Kao, Quận 1, Tp. Hồ Chí Minh</p>



                        <p class="has-medium-font-size"><strong>Điện thoại:</strong> 0965709041</p>



                        <p class="has-medium-font-size"><strong>Email:</strong> support@1kbuy.vn</p>



                        <p class="has-medium-font-size">Công ty tnhh 1KBUY VIỆT NAM (1KBUY) phát triển và cung
                            cấp dịch vụ mới trong lĩnh vực mua sắm online. Chúng tôi tiên phong kết nối khách
                            hàng cùng mua tại các thời điểm khác nhau. Tạo ra giá trị cho mọi thành viên và tiết
                            kiệm tối ưu nhất chi phí mua sắm online.</p>



                        <p class="has-medium-font-size">Tại 1KBUY chúng tôi xây dựng hệ thống đơn giản nhất qua
                            các tài nguyên gần gũi và sẵn có trong mọi lĩnh vực. Giúp khách hàng có những trải
                            nghiệm về sử dụng và dịch vụ tốt nhất, mang đến cho khách hàng phương tiện mua sắm
                            hợp lí nhất, tiết kiệm nhiều chi phí nhất để chi tiêu hiệu quả, thành công.</p>



                        <p class="has-medium-font-size">Chúng tôi đặt lợi nhuận như là cơ sở để phát triển toàn
                            diện hơn nhằm mang đến các dịch vụ đến khách hàng trọn vẹn nhất. Không đặt nặng sự
                            gia tăng lợi nhuận trong khi nghèo sự phục vụ cho khách hàng.</p>



                        <h1>Triết lý kinh doanh</h1>



                        <p class="has-medium-font-size">Càng đơn giản dễ hiểu khách hàng càng gần gũi, khi đó sự
                            phục vụ mới được hiệu quả tự nhiên nhất để đi đến thành công.</p>



                        <h1>Tầm nhìn</h1>



                        <p class="has-medium-font-size">Để mọi khách hàng tiết kiệm tài chính tốt nhất, giảm bớt
                            áp lực chi phí mua sắm.</p>



                        <p class="has-medium-font-size">Xây dựng cộng đồng giá trị, chia sẻ trên nền tảng đơn
                            giản.</p>



                        <h1>Sứ mệnh</h1>



                        <p class="has-medium-font-size">Sẵn sàng phục vụ và cùng khách hàng cải thiện nguồn tài
                            chính ổn định.</p>
                    </div>
                </div>


                <div style="clear:both;"></div>

            </div>
            @include('templates.onekbuy.post-bar')
        </div>
    </div>
</div>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="/asset/onekbuy/style/intro.css">

@endsection
@section('js')
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
        }
      
        function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        }
        $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
        })
      </script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
@endsection