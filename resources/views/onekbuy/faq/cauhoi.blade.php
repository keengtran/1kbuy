@extends('templates.onekbuy.master')
@section('content')
<div class="content">
    <div class="shop-background">
        <div class="container container-shop">
            <div class="row row-intro">
                <div class="col-12 col-md-8 col-xl-9 question-left ">
                    <div class="row-question-left">
                        <div class="entry-content">

                            <p class="text-center has-huge-font-size"><strong><span style="color:#0800a3"
                                        class="has-inline-color">Hướng Dẫn Chung</span></strong></p>



                            <p class="has-medium-font-size"><strong>Hướng dẫn đăng kí đăng nhập 1kbuy.vn</strong>
                            </p>



                            <p style="font-size:18px">Nhằm tạo ra sự đơn giản tiện lợi và an toàn nhất cho thông tin
                                khách hàng khi sử dụng dịch vụ của chúng tôi, website 1kbuy.vn không cần đăng ký và
                                đăng nhập thành viên. Khách hàng sẻ có đủ tư cách thành viên khi bắt đầu đặt lệnh
                                sản phẩm đầu tiên thông qua tài khoản ví MoMo của chúng tôi. Nghĩa là khách hàng chỉ
                                cần sử dụng ví điện tử MoMo của mình là có thể sử dụng tất cả các dịch vụ và sự phục
                                vụ của chúng tôi. Đồng nghĩa khách hàng phải có tài khoản ví MoMo và được liên kết
                                với ngân hàng. Lưu ý rằng dịch vụ của 1KBUY hoàn toàn khác và độc lập với dịch vụ
                                của MOMO hay 1KBUY và MOMO không có bất kì liên hệ hợp tác hoặc quan hệ kinh doanh
                                nào đến thời điểm hướng dẫn này được đưa ra.</p>



                            <p class="has-medium-font-size"><strong>Hướng dẫn sử dụng dịch vụ tại 1KBUY</strong></p>



                            <p style="font-size:18px">Quý khách hàng sử dụng đơn giản nhất theo các bước như sau:
                            </p>



                            <p style="font-size:18px">Truy cập vào website 1kbuy.vn hoặc <a
                                    rel="noreferrer noopener" href="http://1kbuy.vn/" target="_blank">tại đây</a>.
                                Quý khách hàng có thể lướt xem các thông tin, sản phẩm để biết thêm về chúng tôi và
                                dịch vụ được cung cấp. Để tham gia dịch vụ, quý khách hàng tiến hành tìm sản phẩm
                                yêu thích hoặc cần mua được thể hiện trên website.</p>



                            <p style="font-size:18px">Tiếp theo, đặt lệnh sản phẩm bằng tài khoản ví MoMo của quý
                                khách hàng và gửi đến tài khoản ví MoMo của 1KBUY. </p>



                            <p style="font-size:18px">Hệ thống sẻ cập nhật lượt đặt lệnh của khách hàng trên sản
                                phẩm được chọn. Kết quả mua thành công sẻ được chúng tôi thông báo trên website vào
                                lúc 22:30 ngày mỗi ngày. Đồng thời gửi tin nhắn thông báo đến MoMo chat của khách
                                hàng. Khách hàng kiểm tra và gửi yêu cầu nhận sản phẩm đến 1KBUY. Nhân viên 1KBUY sẻ
                                kiểm tra lại thông tin và liên hệ chuyển sản phẩm cho khách hàng. </p>



                            <p style="font-size:18px">Trong trường hợp ngược lại mua không thành công chúng tôi mặc
                                định không thông báo. Nếu đủ điều kiện nhận hoàn tiền, khách hàng có thể đặt lệnh
                                yêu cầu hoàn tiền. Chúng tôi sẻ kiểm tra và thông báo đến khách hàng để xác nhận và
                                chuyển tiền hoàn lại cho khách hàng.</p>



                            <p class="has-medium-font-size"><strong>Hướng dẫn chọn sản phẩm</strong></p>



                            <p style="font-size:18px">Quý khách hàng xem thông tin sản phẩm và chọn sản phẩm yêu
                                thích cần mua, sau đó sao chép “Mã sản phẩm” như hình và gửi đến hệ thống để dặt
                                lệnh cho sản phẩm.</p>



                            <figure class="wp-block-gallery columns-1 is-cropped">
                                <ul class="blocks-gallery-grid">
                                    <li class="blocks-gallery-item">
                                        <figure><img
                                                src="https://1kbuy.vn/wp-content/uploads/2020/04/mã-sản-phẩm.png"
                                                alt="" data-id="805"
                                                data-full-url="https://1kbuy.vn/wp-content/uploads/2020/04/mã-sản-phẩm.png"
                                                data-link="https://1kbuy.vn/cau-hoi-thuong-gap/ma-san-pham/"
                                                class="wp-image-805"
                                                srcset="https://1kbuy.vn/wp-content/uploads/2020/04/mã-sản-phẩm.png 913w, https://1kbuy.vn/wp-content/uploads/2020/04/mã-sản-phẩm-300x167.png 300w, https://1kbuy.vn/wp-content/uploads/2020/04/mã-sản-phẩm-768x428.png 768w, https://1kbuy.vn/wp-content/uploads/2020/04/mã-sản-phẩm-600x335.png 600w">
                                        </figure>
                                    </li>
                                </ul>
                            </figure>



                            <p style="font-size:18px"><strong>Hướng dẫn đặt lệnh sản phẩm và giao dịch qua ví MoMo
                                    của 1KBUY</strong></p>



                            <p style="font-size:18px">Với mã sản phẩm vừa sao chép tại sản phẩm cần mua, khách hàng
                                tiến hành vào ví MoMo cá nhân tại mục chuyển tiền và chuyển 1000 vnđ đến ví MoMo của
                                1KBUY. Lưu ý nội dung chuyển tiền (tin nhắn đến người nhận) chính là mã sản phẩm
                                khách hàng chọn. Mã sản phẩm đúng quy cách là chữ in hoa viết liền không dấu giống
                                như mã sản phẩm trong tên của sản phẩm.</p>



                            <div class="wp-block-image">
                                <figure class="aligncenter size-full"><img
                                        src="https://1kbuy.vn/wp-content/uploads/2020/04/tk-momo-1.jpg" alt=""
                                        class="wp-image-981"
                                        srcset="https://1kbuy.vn/wp-content/uploads/2020/04/tk-momo-1.jpg 720w, https://1kbuy.vn/wp-content/uploads/2020/04/tk-momo-1-142x300.jpg 142w, https://1kbuy.vn/wp-content/uploads/2020/04/tk-momo-1-485x1024.jpg 485w, https://1kbuy.vn/wp-content/uploads/2020/04/tk-momo-1-600x1267.jpg 600w">
                                </figure>
                            </div>



                            <p style="font-size:18px">Như vậy nghĩa là với mỗi lệnh đặt sản phẩm quý khách hàng
                                chuyển tương ứng 1000 vnđ vào tài khoản ví MoMo của 1KBUY với nội dung chuyển tiền
                                là mã sản phẩm được chọn. </p>



                            <p style="font-size:18px">Quý khách hàng có thể kết nối tài khoản ví MoMo của chúng tôi
                                qua quét mã QR tại phần NỀN TẢNG THANH TOÁN dưới chân trang website. Hoặc quét mã QR
                                tại đây</p>



                            <div class="wp-block-image">
                                <figure class="aligncenter size-full"><img
                                        src="https://1kbuy.vn/wp-content/uploads/2020/04/Screenshot_4-1.png" alt=""
                                        class="wp-image-978"></figure>
                            </div>



                            <p class="has-medium-font-size"><strong>Hướng dẫn xem kết quả nhận sản phẩm và lịch sử
                                    hoàn tiền</strong></p>



                            <p style="font-size:18px">Kết quả mua thành công và lịch sử hoàn tiền của tất cả thành
                                viên sẻ được chúng tôi cung cấp tại phân mục <a rel="noreferrer noopener"
                                    href="https://1kbuy.vn/thong-bao/" target="_blank">Thông Báo</a> trên website
                                vào lúc 22:30 mỗi ngày. Ngoài ra, đối với các khách hàng đặt lệnh sản phẩm thành
                                công sẻ có tin nhắn thông báo qua tin nhắn MoMo. Quý khách hàng có thể xem và kiểm
                                tra đối chiếu kết quả. Nếu có bất kì phát sinh nào hãy liên hệ ngay với chúng tôi.
                            </p>



                            <p class="has-medium-font-size"><strong>Hướng dẫn nhận sản phẩm</strong></p>



                            <p style="font-size:18px">Chúng tôi xử lý dữ liệu hệ thống để biết lượt đặt lệnh của
                                khách hàng có mua thành công sản phẩm hay không. Sau đó thông báo trên website và
                                qua MoMo chat để quý khách hàng kiểm tra. Tiếp theo khách hàng lựa chọn sản phẩm
                                muốn nhận và gửi email đến 1kproduct@gmail.com để yêu cầu nhận sản phẩm. Lưu ý chủ
                                đề gửi email là thông tin của khách hàng, phận nội dunhg mail là thông tin người
                                nhận sản phẩm. </p>



                            <figure class="wp-block-image size-large"><img
                                    src="https://1kbuy.vn/wp-content/uploads/2020/06/Screenshot_1.png" alt=""
                                    class="wp-image-3292"
                                    srcset="https://1kbuy.vn/wp-content/uploads/2020/06/Screenshot_1.png 603w, https://1kbuy.vn/wp-content/uploads/2020/06/Screenshot_1-300x141.png 300w, https://1kbuy.vn/wp-content/uploads/2020/06/Screenshot_1-600x283.png 600w">
                            </figure>



                            <p>Nếu đã từng nhận sản phẩm từ 1KBUY và thông tin người nhận không thay đổi thì phần
                                nội dung email có thể để trống.</p>



                            <p>Chúng tôi sẻ gửi yêu cầu xác nhận qua tin nhắn MoMo của khách hàng. Sau đó chuyển sản
                                phẩm theo đúng thông tin đã gửi yêu cầu.</p>



                            <p class="has-medium-font-size"><strong>Hướng dẫn yêu cầu hoàn tiền</strong></p>



                            <p style="font-size:18px">Khi đủ điều kiện hoàn tiền như chính sách hoàn tiền 100% và
                                chính sách sử dụng quý khách hàng tiến hành đặt lệnh yêu cầu rút tiền. Khách hàng
                                gửi email yêu cầu hoàn tiền đến 1kreturn@gmail.com với chủ đề là thông tin khách
                                hàng yêu cầu và nội dung là xác nhận email người gửi yêu cầu.</p>



                            <figure class="wp-block-image size-large"><img
                                    src="http://1kbuy.vn/wp-content/uploads/2020/06/yêu-cầu-hoàn-tiền.png" alt=""
                                    class="wp-image-3296"
                                    srcset="https://1kbuy.vn/wp-content/uploads/2020/06/yêu-cầu-hoàn-tiền.png 600w, https://1kbuy.vn/wp-content/uploads/2020/06/yêu-cầu-hoàn-tiền-300x191.png 300w">
                            </figure>



                            <p style="font-size:18px">Chúng tôi sẻ gửi yêu cầu xác nhận qua tin nhắn MoMo của khách
                                hàng. Sau đó chuyển sản phẩm theo đúng thông tin đã gửi yêu cầu.</p>



                            <p class="has-medium-font-size"><strong>Hướng dẫn khiếu nại về sản phẩm, dịch
                                    vụ</strong></p>



                            <p style="font-size:18px">Với bất kì điểm bất cập nào về sản phẩm dịch vụ của chúng tôi
                                quý khách hàng hãy liên hệ với chúng tôi qua email support@1kbuy.vn để khiếu nại hay
                                đóng góp ý kiến. Chúng tôi rất mong muốn được hoàn thiện và phục vụ quý khách hàng
                                một cách gần gũi và tốt nhất có thể.</p>



                            <p style="font-size:18px">Các yêu cầu khiếu nại của quý khách hàng sẻ được phản hồi và
                                cố gắng giải quyết nhanh nhất tùy theo nội dụng khiếu nại và các chính sách của
                                chúng tôi như đã công bố.</p>



                            <p class="has-medium-font-size"><strong>Hướng dẫn hủy sử dụng dịch vụ tại
                                    1KBUY.</strong></p>



                            <p>Khách hàng cần đọc hiểu chính sách hủy dịch vụ của chúng tôi để đảm bảo không thực
                                hiện sai nhằm tránh những thiệt hại có thể xảy ra. Trong trường hợp muốn hủy dịch vụ
                                tại 1KBUY quý khách hàng gửi yêu cầu hủy dịch vụ đến support@1kbuy.vn với chủ đề là
                                thông tin khách hàng và nội dung là lí do hủy dịch vụ.</p>



                            <figure class="wp-block-image size-large"><img
                                    src="https://1kbuy.vn/wp-content/uploads/2020/06/Hủy-dịch-vụ.png" alt=""
                                    class="wp-image-3301"
                                    srcset="https://1kbuy.vn/wp-content/uploads/2020/06/Hủy-dịch-vụ.png 599w, https://1kbuy.vn/wp-content/uploads/2020/06/Hủy-dịch-vụ-300x153.png 300w">
                            </figure>



                            <p class="has-medium-font-size"><strong>Hướng dẫn liên hệ</strong></p>



                            <p style="font-size:18px">Mọi vấn đề của khách hàng thắc mắc đều được ghi nhận qua:</p>



                            <p style="font-size:18px">Email: support@1kbuy.vn – 24/7/365</p>



                            <p style="font-size:18px">Hotline: 24/7/365 Từ 8:00 đến 22:00</p>



                            <p style="font-size:18px">LiveChat: 24/7/365 Từ 8:00 đến 22:00</p>



                            <p class="has-medium-font-size"><strong>Hướng dẫn kiếm tiền với 1KBUY</strong></p>



                            <p style="font-size:18px">Giá sản phẩm của chúng tôi được đưa ra dựa trên giá thị
                                trường. Do đó khách hàng hoàn toàn có thể đặt mua sản phẩm với giá gần như miễn phí
                                hoặc rất thấp và bán lại cho người khác để kiếm lợi nhuận. Hoặc có thể nhận các giao
                                dịch tự do để mua giúp thay cho thành viên có nhu cầu mà không có nhiều thời gian.
                                Tuy nhiên, nếu thực hiện kiếm tiền bằng cách này khách hàng lưu ý đọc kĩ và hiểu rõ
                                các quy tắc chính sách của 1KBUY.</p>



                            <p style="font-size:18px">Chúng tôi trân trọng cảm ơn quý khách hàng đã quan tâm và sử
                                dụng dịch vụ. Kính chúc quý khách hàng có những trải nghiệm thú vị và đặt mua sản
                                phẩm thành công. Mong được phục vụ quý khách hàng với mức độ hài lòng nhiều nhất về
                                dịch vụ của chúng tôi.</p>
                        </div>
                    </div>


                    <div style="clear:both;"></div>

                </div>
                @include('templates.onekbuy.post-bar')
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="/asset/onekbuy/style/cauhoi.css">

@endsection
@section('js')
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
        }
      
        function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        }
        $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
        })
      </script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
@endsection