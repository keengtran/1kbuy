@extends('templates.onekbuy.master')
@section('content')
<div class="content">
    <div class="shop-background">
        <div class="container container-shop">
            <div class="row row-intro">
                <div class="col-12 col-lg-8 col-xl-9 intro-left ">
                    <div class="row-intro-left">
                        <div class="entry-content">
                
                            <p class="text-center has-huge-font-size"><strong><span style="color:#0300a3" class="has-inline-color">Chính Sách Bảo Mật Thông Tin</span></strong></p>
                            
                            
                            
                            <h3>1. Mục đích và phạm vi thu thập thông tin</h3>
                            
                            
                            
                            <p style="font-size:18px">Công ty tnhh 1KBUY VIỆT NAM (sau đây gọi là “1KBUY”,  “Chúng tôi”) sẽ thu thập các thông tin cá nhân cần thiết của khách hàng như Họ và Tên, địa chỉ thư điện tử, số điện thoại, địa chỉ nhận sản phẩm, để chúng tôi cung cấp dịch vụ, xác thực và quản lý dịch vụ dựa theo thông tin của khách hàng.</p>
                            
                            
                            
                            <h3 id="2-cam-kết-và-phạm-vi-sử-dụng-thông-tin">2. Cam kết và Phạm vi sử dụng thông tin</h3>
                            
                            
                            
                            <p style="font-size:18px">Chúng tôi cam kết chỉ sử dụng thông tin cá nhân của khách hàng trong phạm vi nội bộ để chăm sóc khách hàng tốt nhất.</p>
                            
                            
                            
                            <p style="font-size:18px">Trong trường hợp cần thiết, Chúng tôi sẽ sử dụng các thông tin này để gửi thông báo khuyến mãi, cảm ơn, các thông báo khẩn cấp thông qua tài khoản ví MoMo, email, điện thoại hoặc thư tín.</p>
                            
                            
                            
                            <h3>3. Bảo mật dữ liệu trên tài khoản ví MoMo</h3>
                            
                            
                            
                            <p style="font-size:18px">Khi khách hàng sử dụng các dịch vụ của chúng tôi thông qua ví điện tử MoMo như đặt lệnh sản phẩm, nhận hoàn tiền chúng tôi cam kết không can thiệp tài khoản ví MoMo của khách hàng khi không được sự cho phép. 1KBUY cũng không chịu trách nhiệm các mất mát dữ liệu liên quan đến sự quản lý lỏng lẻo hoặc xóa nhầm của khách hàng.</p>
                            
                            
                            
                            <h3 id="3-thời-gian-lưu-trữ-thông-tin">4. Thời gian lưu trữ thông tin</h3>
                            
                            
                            
                            <p style="font-size:18px">Chúng tôi sẽ bắt đầu lưu trữ các thông tin của khách hàng kể từ lúc khách hàng phát sinh lượt đặt lệnh sản phẩm đầu tiên trên tài khoản ví MoMo của chúng tôi. Việc thu thập thông tin này sẽ diễn ra đến khi Chúng tôi hoàn thành mục đích thu thập thông tin và khách hàng có yêu cầu xóa thông tin.</p>
                            
                            
                            
                            <h3 id="4-địa-chỉ-đơn-vị-thu-thập-và-quản-lý-thông-tin">5. Địa chỉ đơn vị thu thập và quản lý thông tin</h3>
                            
                            
                            
                            <p style="font-size:18px">Công ty tnhh 1KBUY VIỆT NAM</p>
                            
                            
                            
                            <p style="font-size:18px">Địa chỉ: Tầng 1, số 4 Nguyễn Thị Minh Khai, Phường Đa Kao, Quận 1, Tp. Hồ Chí Minh, Việt Nam.</p>
                            
                            
                            
                            <p style="font-size:18px">SĐT: 0965709041</p>
                            
                            
                            
                            <h3><strong> 6.  Trách nhiệm của khách hàng</strong></h3>
                            
                            
                            
                            <p style="font-size:18px">Khách hàng có trách nhiệm khai báo thông tin đầy đủ và chính xác khi được yêu cầu. Quý khách hàng có trách nhiệm tự quản lý thông tin và tài khoản ví MoMo cá nhân.</p>
                            
                            
                            
                            <p style="font-size:18px">Khi phát hiện truy cập bất hợp pháp, khách hàng phải có trách nhiệm thông báo ngay đến bộ phận hỗ trợ khách hàng của Chúng tôi qua thư điện tử support@1kbuy.vn để chúng tôi xử lý kịp thời.</p>
                                            </div>
                    </div>


                    <div style="clear:both;"></div>

                </div>
                @include('templates.onekbuy.post-bar')
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
        }
      
        function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        }
        $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
        })
      </script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
@endsection