@extends('templates.onekbuy.master')
@section('content')
<div class="content">
    <div class="shop-background">
        <div class="container container-shop">
            <div class="row row-intro">
                <div class="col-12 col-lg-8 col-xl-9 intro-left ">
                    <div class="row-intro-left">
                        <div class="entry-content">

                            <p class="has-huge-font-size"><strong><span style="color:#0d00a3"
                                        class="has-inline-color">Chính Sách Sản Phẩm, Dịch Vụ-Bồi
                                        Thường</span></strong></p>



                            <h3><strong>Cam kết chất lượng dịch vụ</strong></h3>



                            <p style="font-size:18px">Đây là cam kết về chất lượng dịch vụ mà 1KBUY VIỆT NAM (sau
                                đây gọi là “1KBUY”, “chúng tôi”) dành cho khách hàng của mình. Điều này bao gồm sự
                                phục vụ cho tất cả các khách hàng đang sử dụng, hoặc có nhu cầu muốn sử dụng dịch vụ
                                của 1KBUY trong tương lai. Cam kết này cũng quy định trách nhiệm và bồi thường của
                                chúng tôi khi không đáp ứng được yêu cầu dịch vụ được nêu trong chính sách sử dụng.
                                Bản cam kết nhằm thể hiện rõ mong muốn tạo sự tin tưởng và giúp khách hàng yên tâm
                                hơn khi sử dụng dịch vụ.</p>



                            <h3><strong>Chất lượng sản phẩm</strong></h3>



                            <p style="font-size:18px">Sản phẩm được sử dụng để làm cơ sở cho khách hàng đặt lệnh
                                trên website 1kbuy.vn tuân theo quy định về xuất xứ và chất lượng hàng hóa của Việt
                                Nam. Chúng tôi luôn cố gắng kiểm tra tất cả các sản phẩm mà 1KBUY sử dụng để đảm bảo
                                nguồn sản phẩm có chất lượng tốt nhất cho quý khách hàng. Sự kiểm tra sản phẩm bao
                                gồm cả nguồn gốc xuất sứ, mức độ an toàn, tính thẩm mỹ thời trang hợp xu hướng, thời
                                hạn sử dụng…Đặc biệt là giá cả sản phẩm đúng với giá trị trên thị trường? Ngoài ra
                                chúng tôi sẻ cập nhật và kiểm tra thêm nhiều tiêu chí khác trên đặc tính của sản
                                phẩm hoặc các sản phẩm mà 1KBUY để cung cấp cho khách hàng trong tương lai.</p>



                            <h3><strong>Xác định vị trí đặt mua sản phẩm thành công</strong></h3>



                            <p style="font-size:18px">Để đảm bảo bí mật thông tin kinh doanh và minh bạch cho khách
                                hàng chúng tôi nêu ra các nội dung quy trình như sau:</p>



                            <p style="font-size:18px">Sản phẩm được chọn để cung cấp cho khách hàng khi đặt lệnh
                                thành công được dựa trên mã sản phẩm mà khách hàng đặt lênh. Sản phẩm được đặt trước
                                thì chọn trước, sản phẩm sau thì chọn sau, sản phẩm nào chọn rồi sẻ bị bỏ ra khỏi
                                danh sách sản phẩm đặt lệnh thành công. Nếu trong ngày mà sử dụng hết tất cả sản
                                phẩm được đặt lệnh thì quay lại chu kỳ mới.</p>



                            <p style="font-size:18px">Lượt đặt lệnh mua của khách hàng có thể rơi vào vị trí bất kì,
                                tuy nhiên nếu vị trí đó trùng với giá trị sản phẩm được chọn ở trên thì được xem đặt
                                lệnh mua thành công. Ví dụ khách hàng đặt lệnh sản phẩm 1K00001 có giá trị 789.000
                                VND nhưng lượt đặt rơi vào vị trí thứ 568 của sản phâm 1K00019 (được chọn) có giá
                                trị 568.000 VND thì khách hàng nhận được sản phẩm 1K00019.</p>



                            <h3><strong>Bồi thường</strong></h3>



                            <p style="font-size:18px">Chúng tôi cam kết bồi thường trong các trường hợp thỏa thuận
                                sau:</p>



                            <p class="has-medium-font-size"> <strong><em>Về sản phẩm:</em></strong></p>



                            <p style="font-size:18px">Khách hàng nhận sai sản phẩm, sản phẩm không đảm bảo chất
                                lượng, sai sót về hạn sử dụng. Hoặc sản phẩm bị hư hỏng do bên đóng gói, bên giao
                                hàng. Hoặc các nguyên nhân khác mà không phải lỗi sử dụng của khách hàng. Sản phẩm
                                được đổi là sản phẩm thay thế giống với sản phẩm mà khách hàng chọn hoặc sản phẩm
                                khác có giá trị tương đương. Khách hàng không được quy đổi ra tiền mặt và các hiện
                                vật hiện kim có giá trị nào khác. Khi bên chúng tôi chuyển sản phẩm sai địa chỉ nhận
                                hàng mà quý khách hàng cung cấp, 1KBUY có trách nhiệm chuyển lại sản phẩm đó ngay
                                sau khi nhận phản hồi của khách hàng. Trong vòng 12h kể từ khi nhận sản phẩm từ
                                chúng tôi, khách hàng có trách nhiệm kiểm tra sản phẩm và thông báo nếu có lỗi phát
                                sinh. Sau thời gian này chúng tôi không hỗ trợ khiếu nại và giải quyết bồi thường
                                như quy định.</p>



                            <p style="font-size:18px">Trong trường hợp sản phẩm khách hàng đặt mua tạm hết hàng
                                trong kho nhưng khách hàng không muốn đợi nhập sản phẩm đó về, chúng tôi sẻ cung cấp
                                cho khách hàng sản phẩm có giá trị tương đương. (Khách hàng cũng có thể yêu cầu sản
                                phẩm tương đương bất kì nếu như có nhiều lựa chọn sản phẩm thay thế).</p>



                            <p class="has-medium-font-size"> <strong><em>Về hoàn tiền</em></strong>:</p>



                            <p style="font-size:18px">Chúng tôi hoàn tiền sai người nhận hoặc số tiền hoàn lại không
                                đủ như yêu cầu của khách hàng phù hợp với chính sách hoàn tiền 100%, chính sách sử
                                dụng. 1KBUY có trách nhiệm kiểm tra và bồi thường hoặc chuyển thêm đủ số tiền cần
                                hoàn lại cho khách hàng trong vòng 12h kể từ khi nhận phản hồi từ khách hàng. Tiền
                                bồi thường hoàn lại hoặc bổ sung đủ yêu cầu hoàn tiền của khách hàng sẻ được chuyển
                                qua tài khoản ví MoMo của khách hàng. Không thu phí chuyển tiền hoặc bất cứ yêu cầu
                                tài chính nào khác.</p>



                            <p style="font-size:18px">Không có trách nhiệm hoàn tiền hoặc bồi thường cho những lệnh
                                đặt nếu thực hiện sai quy cách đặt lệnh.</p>



                            <h3><strong>Trường hợp loại trừ</strong></h3>



                            <p style="font-size:18px">Cam kết chất lượng dịch vụ và thời gian thực hiện bồi thường
                                bao gồm hay loại trừ các trường hợp sau:</p>



                            <p style="font-size:18px">Hệ thống máy chủ và website bị gián đoạn do các yếu tố khách
                                quan đến từ bên ngoài nổ lực cung cấp dịch vụ của 1KBUY. Đó có thể do nhà cung cấp,
                                do bão trì hệ thống dữ liệu…Và các nguyên nhân liên quan khác.</p>



                            <p style="font-size:18px">Khách hàng không thể liên hệ phản hồi được với chúng tôi mà
                                nguyên nhân đến từ khách hàng như không kết nối internet, không thể đăng nhập email
                                hoặc tài khoản ví MoMo, cung cấp sai thông tin…Và các nội dung khác được quy định
                                trong chính sách sử dụng.</p>



                            <p style="font-size:18px">Các trường hợp mà nguyên nhân phát sinh liên quan đến thiết bị
                                hay phần mềm, công nghệ khác của khách hàng hoặc thiết bị hay phần mềm, công nghệ
                                khác của nhà cung cấp dịch vụ ví điện tử MoMo.</p>



                            <p style="font-size:18px">Các trường hợp gây ra bởi các yếu tố ngoài tầm kiểm soát của
                                chúng tôi như chiến tranh, hỏa hoạn, lũ lụt, khủng bổ, cấm vận, yêu cầu của cơ quan
                                quản lý nhà nước, các tấn công vào hệ thống 1KBUY hoặc các sự cố mạng quốc gia.</p>



                            <h3><strong>Cam kết hỗ trợ</strong></h3>



                            <p style="font-size:18px">Cam kết hỗ trợ khách hàng theo thời gian hỗ trợ cam kết tương
                                ứng theo Chính sách hỗ trợ khách hàng của chúng tôi.</p>



                            <p style="font-size:18px">Thời gian hỗ trợ cam kết: Tất cả các ngày trong năm bao gồm cả
                                lễ tết và các ngày cuối tuần.</p>



                            <ul>
                                <li>Email: support@1kbuy.vn – 24/7/365</li>
                                <li>Live chat tại trang chủ – 24/7/365 Từ 8:00 đến 22:00</li>
                                <li>Hotline – 24/7/365 Từ 8:00 đến 22:00</li>
                            </ul>



                            <h3><strong>Bảo trì cập nhật dữ liệu </strong></h3>



                            <p style="font-size:18px">Bảo trì cập nhật dữ liệu là công việc thường xuyên thực hiện
                                để đảm bảo tính liên tục và chính xác của dịch vụ được cung cấp đến Khách hàng. Việc
                                bảo trì có thể được lên kế hoạch thực hiện vào bất kỳ ngày nào trong tuần (bao gồm
                                cả ngày cuối tuần) và có thể vào bất kỳ thời điểm nào trong ngày. Nếu có bất kì trì
                                hoãn nào ảnh hưởng đến việc cung cấp dịch vụ chúng tôi sẻ thông báo đến khách hàng
                                tại <a rel="noreferrer noopener" href="https://1kbuy.vn/thong-bao/"
                                    target="_blank">Thông báo</a> trên website. Tuy nhiên, 1KBUY sẽ nỗ lực hết sức
                                để không làm ảnh hưởng đến khách hàng.</p>
                        </div>
                    </div>


                    <div style="clear:both;"></div>

                </div>
                @include('templates.onekbuy.post-bar')
            </div>
        </div>
    </div>
</div>

@endsection
@section('js')
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
        }
      
        function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        }
        $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
        })
      </script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
@endsection