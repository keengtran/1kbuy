@extends('templates.onekbuy.master')
@section('content')
<div class="content">
    <div class="shop-background">
        <div class="container container-shop">
            <div class="row row-intro">
                <div class="col-12 col-lg-8 col-xl-9 intro-left ">
                    <div class="row-intro-left">
                        <div class="entry-content">

                            <p class="text-center has-huge-font-size"><strong><span style="color:#1200a3"
                                        class="has-inline-color">Điều Khoản Chung</span></strong></p>



                            <p style="font-size:18px">Công ty tnhh 1KBUY VIỆT NAM (sau đây xin gọi là “1KBUY”,
                                “chúng tôi” tùy theo trường hợp) xin gửi đến quý khách hàng những điều khoản thỏa
                                thuận sử dụng dịch vụ. Khách hàng sử dụng các dịch vụ của 1KBUY đồng nghĩa với việc
                                chấp thuận các điều khoản thỏa thuận rõ ràng trong văn bản này (Điều khoản sử dụng
                                dịch vụ).</p>



                            <p style="font-size:18px"><strong>Quy định sử dụng dịch vụ.</strong><br>Chúng tôi có
                                quyền hủy tư cách thành viên của khách hàng hoặc từ chối phục vụ trong tương lai nếu
                                vi phạm các quy định sử dụng sau đây:</p>



                            <p style="font-size:18px">Xâm phạm nhầm phá hoại website www.1kbuy.vn với bất kì hình
                                thức nào. Kể cả việc sử dụng hình ảnh, nội dung thông tin, cách thức hoạt động kinh
                                doanh của chúng tôi để thực hiện các hành vi vi phạm pháp luật của Nhà nước CHXHCN
                                Việt Nam hiện hành và các quy định quốc tế đang được áp dụng tại Việt Nam.</p>



                            <p style="font-size:18px">Sử dụng truyền bá thông tin phá hoại, sai sự thật có liên quan
                                đến 1KBUY để gây ảnh hưởng xấu hoặc thiệt hại cho nhà cung cấp vận hành ví điện tử
                                MoMo.</p>



                            <p style="font-size:18px">Sử dụng các công cụ để truyền bá, lôi kéo người khác sử dụng
                                dịch vụ của chúng tôi vào các mục đích nhạy cảm, đồi trụy, ảnh hưởng đến thuần phong
                                mỹ tục Việt Nam. </p>



                            <p style="font-size:18px">Sử dụng nhiều hơn một tài khoản ví MoMo để tham gia sử dụng
                                dịch vụ.(Mỗi thành viên chỉ được sử dụng một tài khoản ví MoMo duy nhất. Nếu sử dụng
                                từ 2 tài khoản có cùng thông tin khách hàng sẻ bị hủy tư cách thành viên các tài
                                khoản sau mà chúng tôi không phải cung cấp sản phẩm hoặc hoàn tiền).</p>



                            <p style="font-size:18px">Sử dụng tài khoản ví MoMo để spam hoặc có các hành vi khác
                                ngoài mục đích sử dụng dịch vụ để gây ảnh hưởng, phá hoại tài khoản ví MoMo của
                                1KBUY.</p>



                            <p style="font-size:18px">Thành lập các nhóm để cùng sử dụng, gian lận chính sách sử
                                dụng, chính sách sản phẩm và chính sách hoàn tiền của chúng tôi. Như cùng lúc sử
                                dụng nhiều tài khoản ví điện tử MoMo để đặt nhiều vị trí sản phẩm trên hệ thống nhằm
                                gian lận quyền lợi của các thành viên khác. Vì vậy chúng tôi phải quy định rằng cần
                                có trên 10 tài khoản thành viên tham gia đặt lệnh thì sản phẩm mới được công nhận
                                mua thành công.</p>



                            <p style="font-size:18px">Thông báo các thông tin về sản phẩm trên website của chúng tôi
                                đến người khác với mục đích lừa đảo để thu hút sự chú ý…Và bất kì hình thức liên
                                quan đến gian lận, lừa đảo khác.</p>



                            <p style="font-size:18px"><strong>Trách nhiệm của khách hàng.</strong><br>Khách hàng tại
                                1KBUY luôn có đầy đủ các quyền và nghĩa vụ của bên sử dụng dịch vụ – theo quy định
                                tại Bộ luật dân sự và Luật thương mại Việt Nam. Để đảm bảo quyền lợi tốt nhất, khách
                                hàng cũng cần tuân thủ một số quy định sau đây:</p>



                            <p style="font-size:18px">Khách hàng có trách nhiệm tự bảo mật thông tin cá nhân, thông
                                tin tài khoản ví MoMo, thông tin và truy cập vào email sử dụng để liên hệ với chúng
                                tôi, thông tin sử dụng ví MoMo để đặt lệnh sản phẩm, thông tin thông báo nhận sản
                                phẩm, thông tin liên quan về hoàn tiền cho quý khách từ tài khoản ví MoMo của chúng
                                tôi. Khách hàng có trách nhiệm thông báo với chúng tôi nếu phát hiện hành vi sử
                                dụng, chia sẻ các thông tin này trái phép.</p>



                            <p style="font-size:18px">Khách hàng tự chịu trách nhiệm quản lý tất cả các thông tin
                                giao dịch, tin nhắn giữa khách hàng và chúng tôi trên ví MoMo hoặc qua email liên
                                hệ. Lưu trữ các thông tin cần thiết đối với khách hàng để sử dụng khi có tranh chấp
                                khiếu nại.</p>



                            <p style="font-size:18px">Khách hàng có trách nhiệm kiểm tra thông tin trên tài khoản ví
                                MoMo như các giao dịch và tin nhắn, hoặc email để biết thông tin nhận sản phẩm và
                                thông tin hoàn tiền. Có trách nhiệm quản lý sản phẩm và tiền hoàn lại từ chúng tôi
                                cho các mục đích cá nhân.</p>



                            <p style="font-size:18px">Khách hàng tự chịu trách nhiệm về khả năng tài chính và phân
                                bổ tài chính hợp lí khi đặt lệnh sản phẩm bằng tài khoản ví MoMo cá nhân.</p>



                            <p style="font-size:18px">Khách hàng tự chịu trách nhiệm trên giá trị giao dịch đặt lệnh
                                sản phẩm đến tài khoản ví MoMo của chúng tôi. Cố định giá trị trên mỗi lượt đặt lệnh
                                trên sản phẩm bất kì là 1000 vnd (một nghìn đồng).</p>



                            <p style="font-size:18px">Khách hàng có trách nhiệm cung cấp thông tin chính xác về số
                                điện thoại, tên người nhận và địa chỉ người nhận sản phẩm từ chúng tôi. Trong trường
                                hợp cung cấp sai thông tin chúng tôi không có trách nhiệm giải quyết khiếu nại nhầm
                                chuyển lại sản phẩm khác thay thế cho sản phẩm đã chuyển sai thông tin.</p>



                            <p style="font-size:18px">Đặc biệt quý khách hàng cần có đủ năng lực hành vi dân sự để
                                biết chắc chắn việc nhà cung cấp vận hành dịch ví điện tử MoMo không phải là bên thứ
                                ba hợp tác với 1KBUY để cung cấp dịch vụ. 1KBUY và MOMO hoàn toàn độc lập trong việc
                                hoạt động và cung cấp dịch vụ cho khách hàng.</p>



                            <p style="font-size:18px"><strong>Trách nhiệm của 1KBUY</strong><br>Với tư cách là nhà
                                cung cấp dịch vụ, 1KBUY có đầy đủ các quyền và nghĩa vụ của bên cung ứng dịch vụ
                                theo quy định tại Bộ luật dân sự và Luật thương mại. 1KBUY có trách nhiệm dưới đây:
                            </p>



                            <p style="font-size:18px">Cung cấp dịch vụ với các thông tin, sản phẩm đúng như cam kết
                                trên website của chúng tôi.</p>



                            <p style="font-size:18px">Có trách nhiệm hỗ trợ khách hàng xuyên suốt trong thời gian sử
                                dụng dịch vụ tùy theo chính sách hỗ trợ theo điều khoản sử dụng dịch vụ.</p>



                            <p style="font-size:18px">Thông báo đầy đủ cho khách hàng khi có phát sinh về quyền nhận
                                sản phẩm và hoàn tiền. Thực hiện cung cấp sản phẩm và hoàn tiền khi nhận yêu cầu của
                                khách hàng.</p>



                            <p style="font-size:18px">Thực hiện đúng với các cam kết khác trong văn bản này và những
                                văn bản pháp lý liên quan.</p>



                            <p style="font-size:18px"><strong>Tạm ngừng dịch vụ.</strong><br>Dịch vụ của khách hàng
                                sẽ bị tạm ngừng sau khi nhận email thông báo ngừng cung cấp dịch vụ theo các trường
                                hợp dưới đây:</p>



                            <p style="font-size:18px">Thời gian xác nhận thông tin nhận sản phẩm, thông tin hoàn
                                tiền vượt quá 07 ngày kể từ thời điểm có thông báo yêu cầu xác nhận.</p>



                            <p style="font-size:18px">Khách hàng không yêu cầu nhận sản phẩm đặt lệnh trúng trong
                                thời hạn 45 ngày kể từ ngày có thông báo danh sách đặt lệnh thành công trên website.
                            </p>



                            <p style="font-size:18px">Có tranh chấp xảy ra kiện cáo, hoặc chịu sự kiểm tra của pháp
                                luật.</p>



                            <p style="font-size:18px"><strong>Sử dụng tài khoản.</strong><br>Khách hàng tham gia sử
                                dụng dịch vụ của chúng tôi không cần đăng kí tài khoản. Mỗi khách hàng khi sử dụng
                                dịch vụ đồng nghĩa việc trở thành thành viên của chúng tôi. Khách hàng chỉ cần cung
                                cấp các thông tin tài khoản liên quan như email, số điện thoại, địa chỉ, tên người
                                nhận sản phẩm khi khách hàng gửi yêu cầu nhận sảm phẩm hoặc yêu cầu hoàn tiền. Khách
                                hàng chỉ sử dụng tài khoản ví MoMo thành viên để giao dịch, sử dụng dịch vụ đối với
                                tài khoản ví MoMo của chúng tôi. Nếu khách hàng cố tình sử dụng với các mục đích sai
                                phạm khác sẽ được gửi email cảnh báo. Nếu tái phạm chúng tôi có quyền hủy tư cách
                                thành viên của quý khách. Vĩnh viễn không cung cấp sản phẩm, không hoàn tiền và từ
                                chối phục vụ trong tương lai.</p>



                            <p style="font-size:18px"><strong>Đăng ký tài khoản</strong><br>Tài khoản trên hệ thống
                                quản trị dịch vụ là một tài khoản ví MoMo khách hàng tạo ra trong quá trình sử dụng
                                dịch vụ. Tài khoản này sẽ có chức năng theo dõi thời hạn dịch vụ, lịch sử giao dịch
                                và hoàn tiền đặt lệnh sản phẩm, gửi yêu cầu hỗ trợ và sử dụng các chức năng đặc thù
                                của dịch vụ. Mỗi thành viên chỉ được sử dụng một tài khoản ví MoMo duy nhất để tham
                                gia dịch vụ. Tất cả các tài khoản mới nếu phát hiện trùng lập với thông tin tài
                                khoản cũ đều không được chấp nhận cung cấp dịch vụ. Tài khoản đăng ký mới với thông
                                tin sai quy định sẽ bị hủy tư cách thành viên mà không cần thông báo trước. Không
                                thể đăng ký nhiều tài khoản cho một người dùng. Bạn phải cung cấp thông tin xác nhận
                                chính xác và kịp thời với chúng tôi trong quá trình sử dụng dịch vụ của bạn với
                                1KBUY . Chúng tôi có thể gửi xác nhận bất kỳ thông tin thay đổi nào, và nếu không
                                thể nhận được xác nhận, chúng tôi có quyền tạm ngưng hoặc chấm dứt dịch vụ của bạn.
                            </p>



                            <p style="font-size:18px"><strong>Hủy dịch vụ</strong><br>Bạn phải gửi một yêu cầu hủy
                                dịch vụ từ tài khoản ví MoMo để ngừng sử dụng dịch vụ tại 1KBUY bất cứ lúc nào với
                                bất kỳ lý do nào. Tất cả các khoản tiền trong tài khoản sẽ bị tịch thu tại thời điểm
                                hủy. Bạn có thể yêu cầu chúng tôi xóa tất cả các thông tin cá nhân của bạn tại thời
                                điểm hủy. Mọi dữ liệu của bạn trên dịch vụ của sẽ được xóa cùng lúc khi hủy dịch vụ.
                            </p>



                            <p style="font-size:18px"><strong>Thông tin hóa đơn và thanh toán.</strong></p>



                            <p style="font-size:18px"><strong>Hình thức thanh toán</strong><br>Chỉ hỗ trợ giao dịch
                                đặt lệnh sản phẩm và hoàn tiền trên tài khoản ví MoMo. Trong trường hợp bất khả
                                kháng như tài khoản ví MoMo của khách hàng và 1KBUY bị lỗi (có thông báo) thì có thể
                                áp dụng bù tiền sản phẩm và hoàn tiền qua tài khoản ngân hàng. Ngoài ra khách hàng
                                tuyệt đối không cung cấp thông tin cá nhân với các hình thức giao dịch thanh toán
                                khác cho bất kì ai kể cả nhân viên của 1KBUY. Quý khách hàng là tổ chức hoặc doanh
                                nghiệp có giao dịch khác sử dụng dịch vụ của chúng tôi, có thể giao dịch thanh toán
                                qua tất cả các hình thức thanh toán khả dụng.</p>



                            <p style="font-size:18px"><strong>Thanh toán tạm thời</strong><br>Chúng tôi cần khách
                                hàng hiểu rằng việc đặt lệnh sản phẩm với giao dịch 1000 vnd trên mỗi lượt đặt sản
                                phẩm là tạm thời nhằm duy trì vị trí của khách hàng tại thời điểm đặt lệnh. Tất cả
                                số tiền sử dụng để đặt lệnh này sẻ được 1KBUY hoàn lại 100% cho khách hàng theo đúng
                                <a rel="noreferrer noopener" href="https://1kbuy.vn/chinh-sach-hoan-tien-100/"
                                    target="_blank">Chính sách hoàn tiền 100%</a> của chúng tôi.</p>



                            <p style="font-size:18px"><strong>Tự động gia hạn</strong><br>Nếu khách hàng tiếp tục
                                đặt lệnh sản phẩm sau khi được hoàn tiền lần cuối cùng tại 1KBUY hoặc sau khi đăng
                                kí hủy dịch vụ thành công thì đồng nghĩa việc tự động gia hạn sử dụng dịch vụ của
                                cúng tôi. Khi đó quý khách vẫn được hỗ trợ sử dụng dịch vụ mà không cần thông báo
                                trước với chúng tôi.</p>



                            <p style="font-size:18px"><strong>Thuế, phí giao dịch thanh toán</strong><br>Khách hàng
                                không có nghĩa vụ chi trả các khoản thuế cho việc sử dụng dịch vụ của 1KBUY. Tất cả
                                giao dịch từ tài khoản ví MoMo giữa khách hàng và 1KBUY đều minh bạch thông qua
                                chính sách phí dịch vụ của nhà cung cấp ví điện tử MoMo. Tuy nhiên MOMO không phải
                                là bên thứ ba cung cấp dịch vụ cho khách hàng. Quý khách không có quyền khiếu nại
                                hay yêu cầu MOMO giải quyết bất kì yêu cầu tranh chấp, khiếu nại giữa khách hàng và
                                chúng tôi nếu có.</p>



                            <p style="font-size:18px"><strong>Phí chuyển hàng, phí hoàn tiền</strong><br>Phí chuyển
                                hàng được thông báo đến khách hàng để lựa chọn đơn vị vận chuyển với phí vận chuyển
                                phù hợp yêu cầu của khách hàng khi mua hàng thành công. Nếu khách hàng nhận sản phẩm
                                qua Viettel Post chúng tôi không thu phí chuyển hàng (Free ship). 1KBUY không thu
                                phí hoàn tiền và hiện tại các giao dịch chuyển nhận tiền giữa các tài khoản ví MoMo
                                hoàn toàn miễn phí. Phí hoàn tiền chỉ phát sinh (nếu có) khi nhà cung cấp ví điện tử
                                MoMo thu phí dịch vụ giao dịch giữa các tài khoản ví MoMo. Chúng tôi sẻ thông báo
                                đến khách hàng nếu có bất kì phát sinh nào.</p>



                            <p style="font-size:18px"><strong>Từ chối hoàn tiền</strong><br>Chúng tôi có quyền từ
                                chối hoàn tiền nếu phát hiện khách hàng có hành vi lừa đảo, gian lận hoặc có dấu
                                hiệu vi phạm hình sự.</p>



                            <p style="font-size:18px"><strong>Hỗ trợ.</strong><br>Bạn có thể gửi vấn đề cần được hỗ
                                trợ qua email support@1kbuy.vn, hotline, live chat, hệ thống hỗ trợ. Trong một số
                                trường hợp, giải quyết vấn đề có thể mất thời gian nên chúng tôi rất tiếc không thể
                                hỗ trợ bạn nhanh hơn. Các vấn đề được giải quyết bởi đội ngũ được phân công, bạn
                                không có quyền yêu cầu giải quyết bởi một thành viên cụ thể, sẽ làm chậm trễ giải
                                quyết các vấn đề của bạn. </p>



                            <p style="font-size:18px"><strong>Khiếu nại.</strong><br>Chúng tôi luôn cố gắng để dịch
                                vụ tốt hơn.&nbsp;Khách hàng có quyền khiếu nại hoặc góp ý với dịch vụ của chúng tôi
                                thông qua địa chỉ email support@1kbuy.vn. Khiếu nại của khách hàng sẽ được chúng tôi
                                phản hồi trong thời gian sớm nhất.</p>



                            <p style="font-size:18px"><strong>Giới hạn trách nhiệm và từ chối đảm
                                    bảo.</strong><br>Trong mọi trường hợp, Chúng tôi sẽ không phải chịu trách nhiệm
                                do tổn thất trực tiếp hoặc hậu quả do khách hàng gây ra. Đặc biệt ngay cả việc mất
                                quyền lợi khách hàng hoặc mất dữ liệu do chủ quan của khách hàng. Khách hàng cũng
                                chấp nhận rằng có quyền miễn trừ đảm bảo trách nhiệm tại các điều khoản trong văn
                                bản này nếu có thỏa thuận riêng với khách hàng. Khách hàng không được quyền miễn trừ
                                trách nhiệm với các điều khoản được nêu trong văn bản này.</p>



                            <p style="font-size:18px"><strong>Bồi thường thiệt hại.</strong><br>Trong trường hợp
                                khách hàng vi phạm điều khoản sử dụng nghiêm trọng dẫn tới tổn thất về hạ tầng, nhân
                                sự của 1KBUY thì phải hoàn toàn chịu mọi chi phí thiệt hại.<br>Trường hợp không đảm
                                bảo được dịch vụ như chuyển sản phẩm và hoàn tiền sai thông tin khách hàng cung cấp
                                1KBUY sẽ bồi thường thông qua <a rel="noreferrer noopener"
                                    href="https://1kbuy.vn/chinh-sach-san-pham-boi-thuong/" target="_blank">Chính
                                    sách sản phẩm-bồi thường</a>. </p>



                            <p style="font-size:18px"><strong>Thay đổi các điều khoản.</strong><br>1KBUY có thể thay
                                đổi điều khoản sử dụng dịch vụ, mọi chính sách hoặc các văn bản pháp lý liên quan
                                khi cần thiết. Các thay đổi sẽ được thông báo tại <a rel="noreferrer noopener"
                                    href="https://1kbuy.vn/thong-bao/" target="_blank">Thông báo</a> khi cần thiết
                                nhưng khách hàng có trách nhiệm thường xuyên theo dõi các văn bản này để đảm bảo
                                luôn thực hiện đúng theo các điều khoản.</p>



                            <p></p>
                        </div>
                    </div>


                    <div style="clear:both;"></div>

                </div>
                @include('templates.onekbuy.post-bar')
            </div>
        </div>
    </div>
</div>
@endsection