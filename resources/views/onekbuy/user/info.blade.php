@extends('templates.onekbuy.master')
@section('content')

    <div class="container">
        <div class="row row-form">
            <div class="col-md-3 d-none d-md-block d-lg-block d-xl-block">
                <div class="nav flex-column nav-pills nav-pills-custom" id="v-pills-tab" role="tablist"
                    aria-orientation="vertical">
                    <a class="nav-link mb-3 p-3 shadow tabinfo {{ session()->has('info')?'active':'' }} {{ session()->get('infos')?'':' active' }}" id="infouser" 
                        href="#info" role="tab" aria-controls="v-pills-home" aria-selected="true" data-toggle="pill">
                        <i class="fa fa-user-circle-o mr-2"></i>
                    <span class="font-weight-bold small text-uppercase">Cập nhật thông tin</span></a>

                    <a class="nav-link mb-3 p-3 shadow tabinfo {{ session()->has('deposit')?'active':'' }}" id="depositrequestuser" 
                        href="#depositrequest" role="tab" aria-controls="v-pills-recharge" aria-selected="true" data-toggle="pill"> 
                        <i class="fa fa-user-circle-o mr-2"></i>
                        <span class="font-weight-bold small text-uppercase">Nạp tiền</span></a>

                    <a class="nav-link mb-3 p-3 shadow tabinfo {{ session()->has('refund')?'active':'' }}" id="refunduser" 
                        href="#refund" role="tab" aria-controls="v-pills-refund" aria-selected="true" data-toggle="pill">
                        <i class="fa fa-user-circle-o mr-2"></i>
                        <span class="font-weight-bold small text-uppercase">Hoàn tiền</span></a>

                    <a class="nav-link mb-3 p-3 shadow tabinfo" id="historyuser" 
                        href="#history" role="tab" aria-controls="v-pills-history" aria-selected="true" data-toggle="pill">
                        <i class="fa fa-user-circle-o mr-2"></i>
                        <span class="font-weight-bold small text-uppercase">Lịch sử đặt lệnh </span></a>

                    <a class="nav-link mb-3 p-3 shadow tabinfo" id="re_passworduser" 
                        href="#re_password" role="tab" aria-controls="v-pills-re_pass" aria-selected="true" data-toggle="pill">
                        <i class="fa fa-calendar-minus-o mr-2"></i>
                        <span class="font-weight-bold small text-uppercase">Đổi mật khẩu</span></a>
                     

                </div>
            </div>

            <div class="col-md-9 d-none d-md-block d-lg-block d-xl-block">
                <div class="tab-content" id="v-pills-tabContent">
                    {{-- tab info  --}}
                    <div class="tab-pane fade shadow rounded bg-white tabcontent 
                    {{ session()->has('info')?'show active':'' }} {{ session()->get('infos')?'':'show active' }} p-4" id="info" role="tabpanel"
                        aria-labelledby="v-pills-home-tab">
                        @include('errors.success')
                        @include('errors.error')

                        <form class="form-update-info" method="post" action="{{ route('onekbuy.user.postinfo')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="formGroupExampleInput">Họ và tên</label>
                                <input type="text" name="fullname" class="form-control"  value="{{ $user->profile->fullname}}">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('fullname') }}</p>
                            </div>
                            <div class="form-group ">
                                <label for="formGroupExampleInput2">Tên hiển thị</label>
                                <input type="text" name="name" class="form-control username" id="formGroupExampleInput2"
                                    value="{{ $user->name}}" >
                                    <p class="help is-danger" style="color:red;">{{ $errors->first('name') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput2">Email</label>
                                <input disabled name="email" type="text" class="form-control email" id="formGroupExampleInput2"
                                    value="{{ $user->email}}" >
                                <p class="help is-danger" style="color:red;">{{ $errors->first('email') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput2">Số điện thoại</label>
                                <input name="phone_number" type="text" class="form-control" id="formGroupExampleInput2" value="{{ $user->profile->phone_number}}">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('phone_number') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Địa chỉ</label>
                                <textarea name="address" class="form-control" id="exampleFormControlTextarea1" rows="3">{{ $user->profile->address}}</textarea>
                                <p class="help is-danger" style="color:red;">{{ $errors->first('address') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="">Ảnh</label><br>
                                <img src="{{ $user->profile->avatar ? asset('./upload/images/'.$user->profile->avatar) : asset('./upload/images/avatar.png') }}"  width="150px" style="object-fit: cover" />
                                <input type="file" class="form-control-file" name="avatar" id="">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('avatar') }}</p>
                            </div>
                            <button type="submit" type="button" class="btn btn-primary">Cập nhật</button>
                        </form>
                    </div>
                    {{-- tab depositrequest --}}
                    <div class="tab-pane fade shadow rounded bg-white tabcontent {{ session()->has('deposit')?'show active':'' }} p-4 " id="depositrequest" role="tabpanel"
                        aria-labelledby="v-pills-recharge-tab">
                        @include('errors.success')
                        <form class="form-reset-pass" method="post" action="{{ route('onekbuy.user.deposit')}}">
                            @csrf
                            <div class="form-group">
                                <label for="formGroupExampleInput">Họ và tên</label>
                                <input name="name" type="text" class="form-control" value="{{ old('name')}}">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('name') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Email</label>
                                <input readonly name="email" type="text" class="form-control" value="{{$user->email}}">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('email') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Số điện thoại</label>
                                <input name="phone" type="text" class="form-control" value="{{ old('phone')}}">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('phone') }}</p>
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">Số tiền</label>
                                <input name="money" type="text" class="form-control" value="{{ old('money')}}">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('money') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Phương thức thanh toán</label>
                                @php
                                    $userId = Auth::guard('web')->user();
                                @endphp
                                <select name="payment" class="form-control" id="mySelect" onchange="myFunction({{ $user->id }})" >
                                    <option>-----------------</option>
                                    <option class="pay_option" value="Momo">Ví Momo</option>
                                    <option class="pay_option" value="VNPay">VNPay</option>
                                    <option class="pay_option" value="Bank">Bank</option>
                                </select>

                                <p id="so-tai-khoan" style="margin:10px 10px"></p>
                                <p id="ten-chu-tai-khoan" style="margin:10px 10px"></p>
                                <p id="noi-dung-chuyen-tien" style="margin:10px 10px"></p>
                                <p id="noi-dung-chuyen-tien2" style="margin:10px 10px"></p>
                                
                                <script>
                                    function myFunction(id) {
                                        var account_number, account_holder, message;
                                        var target_el = event.target.value;
                                        if (target_el == 'Momo') {
                                            account_number = '0981740120'
                                            account_holder = 'Chu tk momo'
                                            message = 'Momo-' + id
                                        } 
                                        if (target_el == 'VNPay') {
                                            account_number = '1111111111'
                                            account_holder = 'Chu tk VNPay'
                                            message = 'VNPay-' + id
                                        } 
                                        if (target_el == 'Bank') {
                                            account_number = '0000000000'
                                            account_holder = 'Chu tk Bank'
                                            message = 'Bank-' + id  
                                        } 
                                            document.getElementById("so-tai-khoan").innerHTML = "Số tài khoản: " + account_number;
                                            document.getElementById("ten-chu-tai-khoan").innerHTML = "Chủ tài khoản: " + account_holder;
                                            document.getElementById("noi-dung-chuyen-tien").innerHTML = "Nội dung chuyển tiền: " + message;
                                            document.getElementById("noi-dung-chuyen-tien2").innerHTML = "<input  id='message' type='hidden' name='message'>";
                                            document.getElementById('message').value  = message;
                                    }
                                </script>
                                
                            </div>
                            <button type="submit" class="btn btn-primary">Cập nhật</button>
                        </form>
                    </div>

                    {{-- tab refund --}}
                    <div class="tab-pane fade shadow rounded bg-white tabcontent {{ session()->has('refund')?'show active':'' }} p-4" id="refund" role="tabpanel"
                        aria-labelledby="v-pills-recharge-tab">
                        @include('errors.success')
                        @include('errors.errors')
                        <form class="form-reset-pass" method="post" action="{{ route('onekbuy.user.refund')}}">
                            @csrf
                            <div class="form-group">
                            <label for="formGroupExampleInput">Họ và tên</label>
                                <input name="name" type="text" class="form-control" value="{{ old('name')}}">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('name') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Số điện thoại</label>
                                <input name="phone" type="text" class="form-control" value="{{ old('phone')}}">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('phone') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Email</label>
                                <input readonly='true' name="email" type="text" class="form-control" value="{{ Auth::guard('web')->user()->email}}">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('email') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Password</label>
                                <input name="password" type="password" class="form-control" value="" >
                                <p class="help is-danger" style="color:red;">{{ Session::has('password') ? Session::get('password') : ''}}</p>
                            </div>
                            <div class="form-group">
                                <label for="refund_value">Số tiền</label>
                                <input name="refund_value" type="text" class="form-control" value="{{ old('refund_value')}}">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('refund_value') }}</p>
                            </div>
                            <button type="submit" class="btn btn-primary">Gửi yêu cầu</button>
                        </form>
                    </div>

                    {{-- lịch sử giao dịch --}}
                    <div class="tab-pane fade shadow rounded bg-white tabcontent p-4" id="history" role="tabpanel"
                        aria-labelledby="v-pills-history-tab">

                        <div class="form-group">
                            <label for="formGroupExampleInput">Số dư: {{ number_format($credit_total)}} vnd</label>
                        </div>

                        <div class="form-group">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Sản phẩm</th>
                                        <th>Giá trị</th>
                                        <th>Thời gian đặt lệnh</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($transactionHistory as $item)
                                        <tr>
                                            <td>{{ $item->id}}</td>
                                            <td>{{ $item->product->name}}</td>
                                            <td>{{ number_format($item->product->promotion_price)}} vnd</td>
                                            <td>{{ $item->created_at->format('H:i:s Y.m.d')}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Id</th>
                                        <th>Sản phẩm</th>
                                        <th>Giá trị</th>
                                        <th>Thời gian đặt lệnh</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>

                    {{-- re_password --}}
                    <div class="tab-pane fade shadow rounded bg-white tabcontent {{ session()->has('re_pass')?'show active':'' }}  p-4 " id="re_password" role="tabpanel"
                        aria-labelledby="v-pills-re_pass-tab">
                        <p class="help is-danger" style="color:red;">{{ session()->get('error')}}</p>
                        @include('errors.success')
                        <form class="form-reset-pass" method="post" action="{{ route('onekbuy.user.re_password')}}">
                            @csrf
                            <div class="form-group">
                                <label for="formGroupExampleInput">Mật khẩu cũ</label>
                                <input type="password" class="form-control" name="old_pass" value="{{ old('old_pass')}}">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('old_pass') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Mật khẩu mới</label>
                                <input type="password" class="form-control" name="new_pass">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('new_pass') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Xác nhận mật khẩu</label>
                                <input type="password" class="form-control" name="cf_pass">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('cf_pass') }}</p>
                            </div>
                            <button type="submit" class="btn btn-primary">Cập nhật</button>
                        </form>
                    </div>

                </div>
            </div>

            {{-- mobile --}}
            <div class="col-12 d-block d-md-none d-lg-none d-xl-none">
                {{-- info  --}}
                <nav class="navbar ">
                    <button class="navbar-toggler my-3 signin-button-custom active" type="button" data-toggle="collapse" data-target="#navbarSupportedContent3" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                      <span>Cập nhập thông tin</span>
                    </button>
                  
                    <div class="collapse navbar-collapse show signin-form-custom mb-3" id="navbarSupportedContent3">
                        @include('errors.success')
                        <form class="form-update-info" method="post" action="{{ route('onekbuy.user.postinfo')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="formGroupExampleInput">Tên hiển thị</label>
                                <input type="text" name="fullname" class="form-control"  value="{{ $user->profile->fullname}}">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('fullname') }}</p>
                            </div>
                            <div class="form-group ">
                                <label for="formGroupExampleInput2">Tên đăng nhập</label>
                                <input type="text" name="name" class="form-control username" id="formGroupExampleInput2"
                                    value="{{ $user->name}}" >
                                    <p class="help is-danger" style="color:red;">{{ $errors->first('name') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput2">Email</label>
                                <input disabled name="email" type="text" class="form-control email" id="formGroupExampleInput2"
                                    value="{{ $user->email}}" >
                                <p class="help is-danger" style="color:red;">{{ $errors->first('email') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput2">Số điện thoại</label>
                                <input name="phone_number" type="text" class="form-control" id="formGroupExampleInput2" value="{{ $user->profile->phone_number}}">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('phone_number') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Địa chỉ</label>
                                <textarea name="address" class="form-control" id="exampleFormControlTextarea1" rows="3">{{ $user->profile->address}}</textarea>
                                <p class="help is-danger" style="color:red;">{{ $errors->first('address') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="">Ảnh</label><br>
                                <input type="image" src="{{ $user->profile->avatar ? asset('./upload/images/'.$user->profile->avatar) : asset('./upload/images/avatar.png') }}"  width="150px" style="object-fit: cover">
                                <input type="file" class="form-control-file" name="avatar" id="">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('avatar') }}</p>
                            </div>
                            <button type="submit" type="button" class="btn btn-primary">Cập nhật</button>
                        </form>
                    </div>
                </nav>

                {{-- depositrequest  --}}
                <nav class="navbar">
                    <button class="navbar-toggler mb-3 signin-button-custom" type="button" data-toggle="collapse" data-target="#navbarSupportedContent4" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                      <span>Nạp tiền</span>
                    </button>
                  
                    <div class="collapse navbar-collapse signin-form-custom mb-3" id="navbarSupportedContent4">
                        @include('errors.success')
                        <form class="form-reset-pass" method="post" action="{{ route('onekbuy.user.deposit')}}">
                            @csrf
                            <div class="form-group">
                                <label for="formGroupExampleInput">Họ và tên</label>
                                <input name="name" type="text" class="form-control" value="{{ old('name')}}">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('name') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Email</label>
                                <input name="email" type="text" class="form-control" value="{{ old('email')}}">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('email') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Số điện thoại</label>
                                <input name="phone" type="text" class="form-control" value="{{ old('phone')}}">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('phone') }}</p>
                            </div>

                            <div class="form-group">
                                <label for="formGroupExampleInput">Phương thức thanh toán</label>
                                <select name="payment" class="form-control" id="mySelect" onchange="myFunction()">
                                    <option>-----------------</option>
                                    <option value="Momo">Ví Momo</option>
                                    <option value="VNPay">VNPay</option>
                                    <option value="Bank">Bank</option>
                                </select>

                                <p id="so-tai-khoan" style="margin:10px 10px"></p>
                                <p id="ten-chu-tai-khoan" style="margin:10px 10px"></p>
                                <p id="noi-dung-chuyen-tien" style="margin:10px 10px"></p>
                                
                                <script>
                                    // function myFunction() {
                                    //     var account_number = '<input type="text" name="account_number">';
                                    //     var money = '<input type="text" name="money">';
                                    //     var message = '<input type="text" name="message">';
                                        
                                    //     document.getElementById("so-tai-khoan").innerHTML = "Số tài khoản: " + account_number;
                                    //     document.getElementById("ten-chu-tai-khoan").innerHTML = "Số tiền: " + money;
                                    //     document.getElementById("noi-dung-chuyen-tien").innerHTML = "Nội dung chuyển tiền: " + message;
                                    // }
                                </script>
                            </div>
                            <button type="submit" class="btn btn-primary">Cập nhật</button>
                        </form>
                    </div>
                </nav>

                {{-- transaction history  --}}
                <nav class="navbar">
                    <button class="navbar-toggler mb-3 signin-button-custom" type="button" data-toggle="collapse" data-target="#navbarSupportedContent8" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                      <span>Hoàn tiền</span>
                    </button>
                  
                    <div class="collapse navbar-collapse signin-form-custom mb-3" id="navbarSupportedContent8">
                        <form class="form-reset-pass" method="post" action="{{ route('onekbuy.user.refund')}}">
                            @csrf
                            <div class="form-group">
                                <label for="formGroupExampleInput">Họ và tên</label>
                                <input name="name" type="text" class="form-control" value="{{ old('name')}}">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('name') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Số điện thoại</label>
                                <input name="phone" type="text" class="form-control" value="{{ old('phone')}}">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('phone') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Email</label>
                                <input name="email" type="text" class="form-control" value="{{ old('email')}}">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('email') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input name="password" type="password" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="password">Số tiền</label>
                                <input name="refund_value" type="text" class="form-control" value="{{ old('refund_value')}}">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('refund_value') }}</p>
                            </div>
                            <button type="submit" class="btn btn-primary">Gửi yêu cầu</button>
                        </form>
                    </div>
                </nav>
                <nav class="navbar">
                    <button class="navbar-toggler mb-3 signin-button-custom" type="button" data-toggle="collapse" data-target="#navbarSupportedContent5" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                      <span>Lịch sử giao dịch</span>
                    </button>
                  
                    <div class="collapse navbar-collapse signin-form-custom mb-3 custom-table" id="navbarSupportedContent5">
                        <div class="form-group">
                            <label for="formGroupExampleInput">Số dư: {{ number_format($credit_total)}} vnd</label>
                        </div>

                        <div class="form-group">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Sản phẩm</th>
                                        <th>Giá trị</th>
                                        <th>Thời gian giao dịch</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($transactionHistory as $item)
                                        <tr>
                                            <td>{{ $item->id}}</td>
                                            <td>{{ $item->product->name}}</td>
                                            <td>{{ number_format($item->product->promotion_price)}} vnd</td>
                                            <td>{{ $item->created_at->format('H:i:s Y.m.d')}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Id</th>
                                        <th>Sản phẩm</th>
                                        <th>Giá trị</th>
                                        <th>Thời gian giao dịch</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </nav>

                {{-- re_password  --}}
                <nav class="navbar">
                    <button class="navbar-toggler mb-3 signin-button-custom" type="button" data-toggle="collapse" data-target="#navbarSupportedContent6" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                      <span>Đổi mật khẩu</span>
                    </button>
                  
                    <div class="collapse navbar-collapse signin-form-custom mb-3" id="navbarSupportedContent6">
                        <p class="help is-danger" style="color:red;">{{ session()->get('error')}}</p>
                        @include('errors.success')
                        <form class="form-reset-pass" method="post" action="{{ route('onekbuy.user.re_password')}}">
                            @csrf
                            <div class="form-group">
                                <label for="formGroupExampleInput">Mật khẩu cũ</label>
                                <input type="password" class="form-control" name="old_pass" value="{{ old('old_pass')}}">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('old_pass') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Mật khẩu mới</label>
                                <input type="password" class="form-control" name="new_pass">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('new_pass') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Xác nhận mật khẩu</label>
                                <input type="password" class="form-control" name="cf_pass">
                                <p class="help is-danger" style="color:red;">{{ $errors->first('cf_pass') }}</p>
                            </div>
                            <button type="submit" class="btn btn-primary">Cập nhật</button>
                        </form>
                    </div>
                </nav>
            
            </div>
        </div>
    </div>
    
    
@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">

@endsection
@section('js')
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
crossorigin="anonymous"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

<script>
    $(document).ready(function () {
        $('#example').DataTable();
    });
    $(document).ready(function(){
            $('.signin-button-custom').click(function() {
                $(".signin-button-custom").removeClass("active");
                $(this).addClass("active");
            });
        });
    
        function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
        }
      
        function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        }
        $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
        })
</script>
{{-- <script>
  $(document).ready(function(){
      alert(location.hash)
    var id = document.URL;
    var tab_id = id + "user";
    $('.tabinfo').removeClass("active");
    // $('.tabcontent').removeClass("active show");
    var url  = id.substr(32); 
    var urluser  = url + 'user';

    $(url).addClass("active");
    $(urluser).addClass("active show");
        
}); 
</script> --}}

@endsection