@extends('/templates.onekbuy.master')
@section('content')
<div class="content">
    <div class="slider-main">
        <div class="background-img-top">
            <div class="container img-top">
              <a href="https://www.facebook.com/01kbuy/"><img src="{{ asset('upload/images/header.jpg')}}" alt="" width="100%"></a>
            </div>
        </div>
      <div class="container slider-bottom">
        <div class="row">
          <div class="col-md-4 col-lg-2 d-none d-md-block d-print-block">
            <p class="all-product"><i class="fas fa-bars"></i> All Products</p>

            <ul>
              @foreach ($categories as $item)
              <li><a href="{{ route('onekbuy.product.index', $item->slug)}}"><i>{{ $item->name}}</i></a></li>
              @endforeach
              {{-- <li><i class="fas fa-tshirt"></i><i><strong> Thời Trang Nam & Nữ</strong></i></li>
              <li><i class="fas fa-baby-carriage"></i><i><strong> Mẹ & Em</strong></i></li>
              <li><i class="fas fa-heart"></i><i><strong> Sức Khỏe & Làm Đẹp</strong></i></li>
              <li><i class="fas fa-shopping-bag"></i><i><strong> Vali & Túi Xách</strong></i></li>
              <li><i class="fas fa-magic"></i><i><strong> Thiết Bị & Phụ Kiện</strong></i></li>
              <li><i class="fas fa-couch"></i><i><strong> Gia Dụng & Nội Thất</strong></i></li>
              <li><i class="fas fa-futbol"></i><i><strong> Thể Thao</strong></i></li>
              <li><i class="fas fa-angle-double-right"></i><i><strong> Khác</strong></i></li> --}}
            </ul>
          </div>
          <div class="col-12 col-md-8 col-lg-7">
            <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
              <!--Indicators-->
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <a href="/blog/tiet-kiem-tu-dong-100/8"><img class="d-block w-100" src="{{ asset('upload/images/Slide-1.jpg')}}" alt="First slide"></a>
                </div>
                <div class="carousel-item">
                  <a href="/blog/hoan-tien-mua-sam-online-100/5"><img class="d-block w-100" src="{{ asset('upload/images/Slide-2.jpg')}}" alt="Second slide"></a>
                </div>
                <div class="carousel-item">
                  <a href="/blog/dang-cap-ai-noi-la-vung-tien-lang-phi/6"><img class="d-block w-100" src="{{ asset('upload/images/Slide-3.jpg')}}" alt="Third slide"></a>
                </div>
              </div>
              <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
              <!--/.Controls-->
            </div>
          </div>
          <div class="col-12 col-lg-3 custom-3">
            <div class="container">
              <div class="row">
                <div class="col-6 col-lg-12"><a href="/shop/ao-lot-nang-nguc-tinh-te-cho-vong-1-them-goi-cam-36"><img class="img-1"src="/asset/onekbuy/image/Product-promotion-1.jpg" alt=""></a></div>
                <div class="col-6 col-lg-12"><a href="/shop/ao-lot-nang-nguc-tinh-te-cho-vong-1-37"><img class="img-2" src="/asset/onekbuy/image/Product-promotion-2.jpg" alt=""></a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <section class="container section-0">
    <div class="category-products-1">
      <div class="products-content-1">
      @foreach ($categories->chunk(5)[0] as $item)
      <div class="products-1">
        <div class="products-image-1">
          <a href="{{ route('onekbuy.product.index', $item->slug)}}">
            <img src="{{ asset('upload/images/' . $item->image)}}" alt="" class="image-box-1" />
          </a>
        </div>
        <div class="products-text-1">
          <div class="products-text-title-1">
            <h4><a href="">{{ $item->name}}</a>
            </h4>
          </div>
          <div class="more-products">
            <p>{{ $item->products->count()}} sản phẩm</p>
          </div>
        </div>
      </div>
      @endforeach
        

      </div>
      <div style="clear:both;"></div>

  </section>

  <!-- -------------------Section2-------------------------- -->

  <section class="container section-1">
    <div class="category-products">
      <div class="products-content">
        <p class=" widget-title" style="color:#000055;margin: 0px 0px 10px 8px;font-size:150%">
          Xem phổ biến</p>
          @foreach ($popularProducts as $item)
            <div class="products">
              <div class="products-image">
                <a href="{{ route('onekbuy.product.product', ['slug' => $item->slug, 'id' => $item->id])}}">
                  <img src="{{ asset('upload/images/'. $item->image)}}" alt=""
                    class="image-box" />
                </a>
              </div>
              <div class="products-text">
                <div class="products-text-name">
                  <a href="{{ route('onekbuy.product.product', ['slug' => $item->slug, 'id' => $item->id])}}" class="custom-color">
                  {{ $item->name}}
                  </a>
                </div>
                <div class="products-text-price">
                  <ins class="products-text-sale-price">
                    {{ number_format( $item->promotion_price)}}<span class="woocommerce-Price-currencySymbol">&#8363;</span>
                  </ins>
                  <del class="products-text-regular-price">
                   {{ number_format( $item->price)}} đ
                  </del>
                </div>
              </div>
            </div>
        @endforeach
      </div>
      <div style="clear:both;"></div>
​
      <div style="text-align: center;"> <a name="" id="" class="btn m-3 custom-xem-them" href={{ route('onekbuy.product.popularproduct') }} role="button">
        <p><strong>Xem thêm</strong></p>
      </a>
      </div>
    </div>
  </section>
​
    <!-- -------------------Section3-------------------------- -->
    <section class="container section-1">
      <div class="category-products">
        <div class="products-content">
          <p class=" widget-title" style="color:#000055;margin: 0px 0px 10px 8px;font-size:150%">
            Được lựa chọn nhiều nhất</p>
            @foreach ($productChooseTheMost as $item)
              <div class="products">
                <div class="products-image">
                  <a href="{{ route('onekbuy.product.product', ['slug' => $item->slug, 'id' => $item->id])}}">
                    <img src="{{ asset('upload/images/'. $item->image)}}" alt=""
                      class="image-box" />
                  </a>
                </div>
                <div class="products-text">
                  <div class="products-text-name">
                    <a href="{{ route('onekbuy.product.product', ['slug' => $item->slug, 'id' => $item->id])}}" class="custom-color">
                    {{ $item->name}}
                  </a>
                  </div>
                  <div class="products-text-price">
                    <ins class="products-text-sale-price">
                      {{ number_format( $item->promotion_price)}}<span class="woocommerce-Price-currencySymbol">&#8363;</span>
                    </ins>
                    <del class="products-text-regular-price">
                     {{ number_format( $item->price)}} đ
                    </del>
                  </div>
                </div>
              </div>
          @endforeach
        </div>
        <div style="clear:both;"></div>
  
        <div style="text-align: center;"> <a name="" id="" class="btn m-3 custom-xem-them" href={{ route('onekbuy.product.mostchosen') }} role="button">
          <p><strong>Xem thêm</strong></p></a>
        </a>
        </div>
      </div>
    </section>
  <!-- --------------Section 4 ------------------------- -->

  <section>
    <div class="container section-3">
        <h2 class="widget-title" style="color:#000055;font-size:24px;margin: 0px 0px 10px 8px;">
        Đề xuất hấp dẫn </h2>
        <div class="row row-products-3">
          @foreach ($productPropose as $item)
            <div class="col-6 col-md-4 col-xl-3 column-products-3">
                <div class="products-3">
                <div class="products-image-3">
                    <a href="{{ route('onekbuy.product.product', ['slug' => $item->slug, 'id' => $item->id])}}">
                    <img src="{{ asset('upload/images/'. $item->image)}}" alt=""
                        class="image-box-3" />
                    </a>
                </div>
                <div class="products-text-3">
                    <div class="products-text-name">
                      <a href="{{ route('onekbuy.product.product', ['slug' => $item->slug, 'id' => $item->id])}}">
                        <h6>{{ $item->name}}</h6>
                      </a>
                    </div>
                    <div class="products-text-price">
                      <ins class="products-text-sale-price">
                        {{ number_format( $item->promotion_price)}}<span class="woocommerce-Price-currencySymbol">&#8363;</span>
                      </ins><br style="d-none d-lg-block d-print-block">
                      <del class="products-text-regular-price">
                        {{ number_format( $item->price)}} đ
                      </del>
                    </div>
                </div>
                </div>
            </div>
        @endforeach
        </div>
        <div style="text-align: center;"> <a name="" id="" class="btn  m-3 custom-xem-them" href={{ route('onekbuy.product.productpropose') }} role="button">
          <p><strong>Xem thêm</strong></p></a>
      </div>
    </div>
  </section>
</div>
@endsection
@section('js')
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
        }
      
        function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        }
        $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
        })
        
      </script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
@endsection