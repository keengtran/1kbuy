@extends('templates.onekbuy.master')
@section('content')
	    <div class="content">
        <div class="shop-background">
            <div class="container container-shop">
                <div class="row row-shop">
                    <div class="col-12 col-lg-8 col-xl-9 shop-left ">
                        <div class="row">
                            @foreach ($productPropose as $item)
                            <div class="col-6 col-sm-6 col-lg-4">
                                    <div class="shop">
                                        <div class="products-image">
                                            <a href={{route('onekbuy.product.product',['id' => $item->id,'slug' => $item->slug]) }}>
                                                <img src={{asset('./upload/images/'.$item->image)}}
                                                    alt="" class="image-box" />
                                            </a>
                                        </div>
                                        <div class="products-text">
                                            <div class="products-text-name">
                                                <a href={{route('onekbuy.product.product',['id' => $item->id,'slug' => $item->slug]) }}><h6>{{$item->name}}</h6></a>
                                            </div>
                                            <div class="products-text-price">
                                                <ins class="products-text-sale-price">
                                                    {{ number_format($item->promotion_price)}}<span class="woocommerce-Price-currencySymbol">&#8363;</span>
                                                </ins>
                                                <del class="products-text-regular-price">
                                                    {{number_format($item->price)}}&#8363;
                                                </del>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach

                        </div>
                        <div style="clear:both;"></div>
                            <ul class="pagination">
                                {{ $productPropose->links() }}
                            </ul>
                    </div>
                    @include('templates.onekbuy.product-bar')
                </div>
            </div>
        </div>  
    </div>
@stop
@section('js')
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
        }
      
        function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        }
        $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
        })
        
      </script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
@endsection