<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\DepositRequest;
use App\Models\Post;
use App\Models\Product;
use App\Models\Information;
use App\Models\Refund;
use App\Models\Notification;
use App\User;

// use App\User;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Schema::defaultStringLength(191);

        $V_productPropose = Product::where('active', 1)->inRandomOrder()->limit(15)->get();
        $V_recentPost = Post::where('active', 1)->orderBy('id','desc')->paginate(5);
        $V_rencentNotification = Notification::orderBy('id','desc')->paginate(4);
        $popularPosts = Post::where('active', 1)->orderBy('view', 'desc')->limit(8)->get();
        $categoryProduct = Category::orderBy('id', 'desc')->with('products')->get();
        $information = Information::find(1)->first();
        $count_refund = Refund::orderBy('id', 'desc')->get();
        $count_depositrequest = DepositRequest::orderBy('id', 'desc')->get();
        $count_products = Product::orderBy('id', 'desc')->get();
        $count_categories = Category::orderBy('id', 'desc')->get();
        $count_post = Post::orderBy('id', 'desc')->get();
        $count_notification = Notification::all();
        $count_user = User::all();
        // $sortByPopularity = Product::where('active', 1)->orderBy('view', 'desc')->paginate(18);
        view()->share([
            'V_productPropose'=> $V_productPropose,
            'V_recentPost' => $V_recentPost,
            'popularPosts' => $popularPosts,
            'categoryProduct' => $categoryProduct,
            'information'=>$information,
            'count_refund' => $count_refund,
            'count_depositrequest' => $count_depositrequest,
            'count_products' => $count_products,
            'count_categories' => $count_categories,
            'count_post' => $count_post,
            'count_notification' => $count_notification,
            'count_user' => $count_user,
            'V_rencentNotification' => $V_rencentNotification,
            // 'sortByPopularity' => $sortByPopularity,
        ]);
    }
}
