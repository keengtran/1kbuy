<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Wallet;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {
        $users = User::paginate(10);
        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        return view('admin.users.add');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:6',
            'email' => 'required|unique:users|email',
            'password' => 'min:8',
            'password_confirmation' => 'same:password|min:8'
        ], [
            'email.required' => 'Không được bỏ trống trường này!',
            'email.email' => 'Phải nhập đúng định dạng email!',
            'name.required' => 'Không được bỏ trống trường này!',
            'name.min:6' => 'Tên phải có ít nhất 6 ký tự!',
            'password.min:8' => 'Mật khẩu phải có ít nhất 8 ký tự!',
            'password_confirmation.same:password' => 'Xác nhận mật khẩu và mật khẩu phải khớp!',
            'password_confirmation.min:8' => 'Mật khẩu phải có ít nhất 8 ký tự!',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {
            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->role = 0;
            $user->save();

            if ($user->wallet == null) {
                $wallet = new Wallet;
                $wallet->credit_user = 0;
                $wallet->credit_total = 0;
                $wallet->user_id = $user->id;
                $wallet->save();
            }
            return back()->with('success', 'Tạo tài khoản thành công');
        }
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('admin.users.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:6',
            'email' => 'required|email',
        ], [
            'email.required' => 'Không được bỏ trống trường này!',
            'name.required' => 'Không được bỏ trống trường này!',
            'name.min:6' => 'Tên phải có ít nhất 6 ký tự!',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator);
        } else {
            $user = User::findOrFail($id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->save();
            return back()->with('success', 'Cập nhật tài khoản thành công');
        }
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return back()->with('success', 'Xóa tài khoản thành công');
    }
}
