<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Information;
use Validator;



class InforController extends Controller
{
    public function edit(){
        $information = Information::find(1)->first();
        return view('admin.informations.edit', compact('information'));
    }

    public function update(Request $request , $id){
        $validator = Validator::make($request->all(),[
            'phone' => 'required',
            'address' => 'required',
            'email'=> 'required|email',
            'description'=>'required',
            'logo' => 'mimes:jpeg,jpg,png,gif|max:10000',
            'header'=>'required',
        ],[
            'phone.required' => 'Bạn cần nhập số điện thoại',
            'address.required' => 'Bạn cần nhập địa chỉ',
            'email.required' => 'Bạn cần nhập email',
            'email.email' => 'Bạn cần nhập đúng định dạng email',
            'description.required' => 'Bạn cần nhập mô tả ngắn',
            'logo.mimes' => 'Bạn chỉ thêm được ảnh jpeg, jpg, png, gif',
            'logo.max' => 'Ảnh chỉ được tới 100000',
            'header.required'=>'Bạn cần nhập vào ô header',
        ]); 
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }    

        $information = Information::find($id)->first();
        $information->phone = $request->phone;
        $information->address = $request->address;
        $information->email = $request->email;
        $information->description = $request->description;

        if($request->hasFile('logo')){
            $file = $request->file('logo');
            $filename = time().$file->getClientOriginalName();
            $file->move('./upload/images/', $filename);
            $information->logo = $filename;
        }
        $information->save();
        return back()->with('success','Bạn đã cập nhập thông tin thành công');
    }
}
