<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Refund;
use App\Models\Wallet;
use Illuminate\Http\Request;

class RefundController extends Controller
{
    public function index()
    {
        $refund = Refund::orderBy('id', 'desc')->paginate(10);
        return view('admin.refund.index', compact('refund')); 
    }

    public function destroy($id)
    {
        $refund = Refund::findOrFail($id);
        $refund->delete();
        return back()->with('message', 'Xóa yêu cầu hoàn tiền thành công!');
    }

    public function active(Request $request)    
    {
        $id = $request->id;
        $refund = Refund::findOrFail($id);
        if ($refund->status == 0) {
            $refund->status = 1;
            $refund->save();
            $wallet = Wallet::where('user_id', $refund->user_id)->first();
            $wallet->credit_total -= $refund->refund_value;
            $wallet->save();
            return '<span class="badge badge-complete">Chấp nhận</span>';
        } else {
            $refund->status= 0;
            $refund->save();
            return '<a href="javascript:void(0)" onclick="getActive('.$id.')" style="cursor: pointer"><span class="badge badge-danger ">Chưa xác nhận</span> </a>';
        }
    }
}
