<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\ImagesProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Imports\ProductsImport;
use App\Exports\ProductsExport;
use App\Models\Post;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller

{
    public function index()
    {
        $products = Product::orderBy('id', 'desc')->paginate(10);
        return view('admin.product.index', compact('products'));
    }
    
    public function create()
    {
        $categories = Category::all();
        return view('admin.product.create', compact('categories'));
    }
    
    public function active(Request $request)
    {
        $id = $request->id;
        $product = Product::findOrFail($id);
        if ($product->active == 0) {
            $product->active = 1;
            $product->save();
            return '<a href="javascript:void(0)" onclick="getActive('.$id.')" style="cursor: pointer"><i class="fa fa-check text-success"></i> </a>';
        } else {
            $product->active= 0;
            $product->save();
            return '<a href="javascript:void(0)" onclick="getActive('.$id.')" style="cursor: pointer"><i class="fa fa-close text-danger"></i> </a>';
        }
    }
    public function activePost(Request $request){
        $id = $request->id;
        $post = Post::findOrFail($id);
        if ($post->active == 0) {
            $post->active = 1;
            $post->save();
            return '<a href="javascript:void(0)" onclick="getActive('.$id.')" style="cursor: pointer"><i class="fa fa-check text-success"></i> </a>';
        } else {
            $post->active= 0;
            $post->save();
            return '<a href="javascript:void(0)" onclick="getActive('.$id.')" style="cursor: pointer"><i class="fa fa-close text-danger"></i> </a>';
        }
    }
    
    public function store(Request $request)
    {   
        $product = new Product();
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:products',
            'description' => 'required',
            'ship'=>'required',
            'price' => 'required',
            'promotion_price' => 'required',
            'image' => 'required',
            'files' => 'required'
        ]);

        if ($validator->fails()) {
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } 
        }

        $product->category_id  = $request->category_id; 
        $product->name  = $request->name; 
        $product->slug  = Str::slug($request->name); 
        $product->description  = $request->description; 
        $product->ship =$request->ship;
        $product->price  = $request->price; 
        $product->promotion_price  = $request->promotion_price; 
  


        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $filename = time().$file->getClientOriginalName();
            $file->move('./upload/images/', $filename);
            $product->image = $filename;
        } 
       
        $product->save();

        if ($request->hasFile('image')) {
            foreach($request->file('files') as $item){
                $addpicture = New ImagesProduct();
                $name= $item->getClientOriginalName();
                $item->move(public_path().'/upload/', $name);  
                $addpicture->image_name = $name;
                $addpicture->alt = $product->name;
                $addpicture->product_id = $product->id;
                $addpicture->save();
            }
        }
        return back()->with('success', 'Thêm sản phẩm thành công!');
    }

    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $categories = Category::all();
        return view('admin.product.edit', compact('product', 'categories'));
    }

    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' => 'unique:products,name,'. $id,
        ]);
            
        if ($validator->fails()) {
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } 
        }

        $product->category_id  = $request->category_id; 
        $product->name  = $request->name; 
        $product->slug  = Str::slug($request->name); 
        $product->description  = $request->description; 
        $product->ship =$request->ship;
        $product->price  = $request->price; 
        $product->promotion_price  = $request->promotion_price; 
        
        if ($request->file('image') != '') {
            $path = public_path('upload/images/');
            $file_old = $path . $product->image;
            if(File::exists($file_old)) {
                File::delete($file_old); 
            }
            
            $file = $request->file('image');
            $filename = time().$file->getClientOriginalName();
            $file->move('./upload/images/', $filename);
            $product->image = $filename;
        } 
        $product->save();
        return redirect()->route('admin.product.index')->with('success', 'Cập nhật sản phẩm thành công!');
    }
    
    public function destroy(Request $request,$id)
    {
        $product = Product::findOrFail($id);
        $path = public_path('upload/images/');
        $file_old = $path . $product->image;
        if(File::exists($file_old)) {
            File::delete($file_old); 
        }
        $product->delete();
        return response()->json([
            'message' => 'Xóa sản phẩm thành công!'
          ]);
    }

    public function importExportView()
    {
       return view('admin.product.import');
    }
   
    public function import() 
    {
        if (!(request()->hasFile('import'))) {
            return back()->with('error', 'Bạn chưa chọn file!');
        }
        Excel::import(new ProductsImport,request()->file('import'));
        return back()->with('success', 'Nhập dữ liệu sản phẩm từ file excel thành công!');
    }

    public function export() 
    {
        return Excel::download(new ProductsExport, 'products.xlsx');
    }

    public function detail($id)
    {
        $product = Product::with('user')->find($id);
        return view('admin.product.detail', compact('product'));
    }
}
