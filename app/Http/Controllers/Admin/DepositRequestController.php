<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DepositRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\User;

class DepositRequestController extends Controller
{
    public function index()
    {
        $depositrequest = DepositRequest::orderBy('id', 'desc')->paginate(10);
        return view('admin.depositrequest.index', compact('depositrequest'));
    }

    public function active(Request $request)
    {
        $id = $request->id;
        $depositrequest = DepositRequest::findOrFail($id);
        if ($depositrequest->status == 0) {
            $depositrequest->status = 1;
            $depositrequest->save();
            $wallet = User::findOrFail($depositrequest->user_id)->wallet;
            $wallet->credit_total += $depositrequest->money;
            $wallet->save();
            $data  = [  
                'id' => $depositrequest->id,
                'name' => $depositrequest->name, 
                'email' => $depositrequest->email, 
                'wallet' => $wallet->credit_total
            ];
            Mail::send('admin.depositrequest.mail',array('data' => $data),function($message) use ($data){
                $message->to($data['email'], $data['name'] )->subject('1kbuy');
            });
            return '<span class="badge badge-complete">Chấp nhận</span>';
        } else {
            $depositrequest->status= 0;
            $depositrequest->save();
            return '<a href="javascript:void(0)" onclick="getActive('.$id.')" style="cursor: pointer"><span class="badge badge-danger">Chưa chấp nhận</span> </a>';
        }
    }

    public function destroy($id)
    {
        $depositrequest = DepositRequest::findOrFail($id);
        $depositrequest->delete();
        return response()->json([
            'message' => 'Xóa yêu cầu nạp tiền thành công!'
        ]);
    }
}
