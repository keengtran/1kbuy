<?php

namespace App\Http\Controllers\Onekbuy;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;

class IndexController extends Controller
{

    public function index()
    {
    	// $cats = Category::limit(5)->get();
        // $itemRandoms = Product::where('active', 1)->inRandomOrder()->limit(28)->get();
        $popularProducts = Product::where('active', 1)->orderBy('view', 'desc')->limit(10)->get();
        $categories = Category::limit(8)->get();
        $productChooseTheMost = Product::where('active', 1)->orderBy('buyer_number', 'desc')->limit(20)->get();
        $productPropose = Product::inRandomOrder()->limit(28)->get();
        return view('onekbuy.index.index', compact('popularProducts', 'categories', 'productChooseTheMost', 'productPropose'));
    }

    public function cauhoi(){
        return view('onekbuy.faq.cauhoi');
    }

    public function gioithieu(){
        return view('onekbuy.faq.gioithieu');
    }

    public function dieukhoan(){
        return view('onekbuy.faq.dieukhoan');
    }

    public function chinhsach(){
        return view('onekbuy.faq.chinhsach');
    }

    public function hoantien(){
        return view('onekbuy.faq.hoantien');
    }

    public function boithuong(){
        return view('onekbuy.faq.boithuong');
    }

    public function vidientu(){
        return view('onekbuy.faq.vidientu');
    }
}

