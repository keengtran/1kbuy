<?php

namespace App\Http\Controllers\Onekbuy;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index(){
        $post = Post::orderBy('id','desc')->paginate(10);
    	return view('onekbuy.post.index',compact('post'));
    }
    public function detail($slug,$id){
        $post = Post::find($id);
        // $postid = Post::orderBy('id','desc')->paginate(4);
        $previous = Post::where('id', '<', $post->id)->orderBy('id','desc')->first();
        $next = Post::where('id', '>', $post->id)->orderBy('id')->first();

        $minutes = '60';
        $key = 'detail' . $post->id;
        if (Cache::has($key)) {
            Cache::put($key, 1, $minutes);
            $post->view +=1;
            $post->save();
        }
        return view('onekbuy.post.detail')->with(compact('post','previous','next'));
    }

    public function searchBlog(Request $request){
        // dd($request->name);
        $search = Post::where('title','LIKE','%'.$request->title.'%')->orderby('id','desc')->paginate(8);
        // dd($search);
        // $search -> name = $request -> name;
        return view('onekbuy.post.search',compact('search'));
    }
}
