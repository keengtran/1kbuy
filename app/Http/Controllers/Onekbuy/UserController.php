<?php

namespace App\Http\Controllers\Onekbuy;

use App\Http\Controllers\Controller;
use App\Models\DepositRequest;
use App\Models\Profile;
use App\Models\Refund;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Models\Product_User;
use App\Models\Wallet;
class UserController extends Controller
{
    public function info(){
        $userId = Auth::guard('web')->user()->id;
        $credit_total = Wallet::with('user')->where('user_id', $userId)->first()->credit_total;
        $user = Auth::user();
        $transactionHistory = Product_User::where('user_id', Auth::guard('web')->user()->id)->get();
        return view('onekbuy.user.info', compact('user', 'transactionHistory', 'credit_total'));
    }

    public function postinfo(Request $request)
    {
        $profile = Profile::where('user_id', Auth::guard('web')->user()->id)->first();
        $user = Auth::guard('web')->user();
        $profile->fullname = $request->fullname;
        $user->name = $request->name;
        $profile->phone_number = $request->phone_number;
        $profile->address = $request->address;
     
   
        if ($request->file('avatar') != '') {
            $path = public_path('upload/images/');
            $file_old = $path . $profile->avatar;
            if(File::exists($file_old)) {
                File::delete($file_old); 
            }
            
            $file = $request->file('avatar');
            $filename = time().$file->getClientOriginalName();
            $file->move('./upload/images/', $filename);
            $profile->avatar = $filename;
        } 
        
        $profile->save();
        $user->save();
        $request->session()->flash('info', true);
        return back()->with('success', 'Cập nhật thông tin thành công!' );
    }

    public function deposit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:6',
            'phone' => 'required|max:10|regex:/(0)[0-9]{9}/',
            'email' => 'required|email',
            'money' => 'required',
            'message' => 'required'
        ], [
            'name.required' => 'Nhập họ và tên của bạn',
            'name.min' => 'Điền đầy đủ họ và tên',
            'phone.required' => 'Nhập số điện thoại của bạn',
            'phone.regex' => 'Định dạng số điện thoại không hợp lệ',
            'email.required' => 'Nhập thông tin Email đăng ký ',
            'email.email' => 'Bạn cần phải nhập đúng định dạng email',
            'money.required' => 'Bạn cần phải số tiền',
            'message.required' => 'Bạn chưa chọn phương thức thanh toán',
        ]);
        if ($validator->fails()) {
            $request->session()->flash('deposit', true);
            $request->session()->flash('infos', true);
            return back()->withErrors($validator)->withInput();
        }
        $userId = Auth::guard('web')->user()->id;
        $depositrequest = new DepositRequest;
        $depositrequest->name = $request->name;
        $depositrequest->phone = $request->phone;
        $depositrequest->email = $request->email;
        $depositrequest->payment = $request->payment;
        $depositrequest->account_number = '';
        $depositrequest->money = $request->money;
        $depositrequest->message = $request->message;
        $depositrequest->user_id = $userId;
        $depositrequest->save();

        $data = $request->all();
        $data['id'] = $depositrequest->id;
        $request->session()->flash('deposit', true);
        $request->session()->flash('infos', true);
        Mail::send('onekbuy.user.mail.depositrequest',array('data' => $data),function($message) use ($data){
            $message->to(Config::get('app.mail_user'), $data['name'] )->subject('Yêu cầu nạp tiền');
        });
        return back()->with('success', 'Đã gửi yêu cầu nạp tiền!');
    }

    public function refund(Request $request)
    {
        $userId = Auth::guard('web')->user()->id;
        $refund = new Refund;
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:6',
            'phone' => 'required|max:10|regex:/(0)[0-9]{9}/',
            'refund_value' => 'required|integer'
        ], [
            'name.required' => 'Bạn cần phải nhập tên!',
            'name.min:6' => 'Bạn cần phải nhập ít nhất 6 ký tự',
            'phone.required' => 'Bạn cần phải nhập số điện thoại',
            'phone.regex' => 'Bạn cần nhập số điện thoại đúng định dạng!',
            'refund_value.required' => 'Bạn cần phải nhập số tiền'
        ]);
        
        if ($validator->fails()) {
            $request->session()->flash('refund', true);
            $request->session()->flash('infos', true);
            return back()->withErrors($validator)->withInput()->with(['active' => true ]);
        }
        $wallet = Wallet::where('user_id', $userId)->first();
        
        if($wallet->credit_total < $request->refund_value){
            $request->session()->flash('refund', true);
            $request->session()->flash('infos', true);
            return back()->with('error', 'Bạn không có đủ tiền trong ví!')->withInput();
        }
        
        $refund->name = $request->name;
        $refund->email = $request->email;
        $refund->refund_value = $request->refund_value;
        $refund->user_id = $userId;
        $refund->status = 0;
        $password = $request->password;
        if(!Hash::check($password, Auth::guard('web')->user()->password)) {
            $request->session()->flash('refund', true);
            $request->session()->flash('infos', true);
            return back()->with('password', 'Mật khẩu không chính xác!')->withInput();
        } 
        $refund->save();
        $data = $request->all();
        $request->session()->flash('refund', true);
        $request->session()->flash('infos', true);
        Mail::send('onekbuy.user.mail.refund',array('data' => $data),function($message) use ($data){
            $message->to(Config::get('app.mail_user'), $data['name'] )->subject('Yêu cầu hoàn tiền');
        });

        return back()->with('success', 'Gửi yêu cầu hoàn tiền thành công!');
    }   

    public function re_password(Request $request)
    {
        $user = Auth::guard('web')->user();
        if(!Hash::check($request->old_pass, Auth::guard('web')->user()->password)) {
            $request->session()->flash('re_pass', true);
            $request->session()->flash('infos', true);
            return back()->with('error', 'Bạn cần phải nhập đúng mật khẩu cũ');
        } 
        $validator = Validator::make($request->all(), [
            'old_pass' => 'required',
            'new_pass' => 'required:min:8',
            'cf_pass' => 'required|same:new_pass'
        ], [
            'old_pass.required' => 'Bạn cần nhập mật khẩu cũ',
            'new_pass.required' => 'Bạn cần nhập mật khẩu mới',
            'cf_pass.required' => 'Bạn cần nhập lại mật khẩu mới',
            'new_pass.min:8' => 'Bạn cần nhập ít nhất là 8 ký tự',
            'new_pass.same:new_pass' => 'Bạn cần nhập đúng mật khẩu mới',
        ]);

        if ($validator->fails()) {
            $request->session()->flash('re_pass', true);
            $request->session()->flash('infos', true);
            return back()->withErrors($validator)->withInput();
        }
        $user->password = bcrypt($request->new_pass);
        $user->save();
        $request->session()->flash('re_pass', true);
        $request->session()->flash('infos', true);
        return back()->with('success', 'Thay đổi mật khẩu thành công');
    }
}