<?php

namespace App\Http\Controllers\Onekbuy;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\Product_User;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\HttpFoundation\Session\Session;
use Auth;
use App\User;
use Illuminate\Support\Facades\Auth as FacadesAuth;

class ProductController extends Controller
{
    // public function index(){
    //     $product = Product::orderBy('id','desc')->paginate(8);
    // 	return view('onekbuy.shop.index',compact('product'));
    // }
    public function index($slug = 'all'){
        if ($slug == 'all'){
            $categoryPro = Product::orderby('id','desc')->paginate(18);
        }
        else {
            $categoryId =Category::where('slug',$slug)->first()->id;
            $categoryPro = Product::where('category_id',$categoryId)->orderBy('id','desc')->paginate(18);
        }
        $countProduct = Product::orderby('id','desc')->get();
        return view('onekbuy.product.index',compact('categoryPro','countProduct'));    
    }

    public function product($slug, $id)
    {
        $product = Product::findOrFail($id); 
        $productRelated = Product::where('category_id', $product->category_id)->inRandomOrder()->paginate(8);
        $productPropose = Product::inRandomOrder()->limit(15)->get();
        $profileId = Auth::guard('web')->user()->id;
        
        $minutes = '60';
        $key = 'detail' . $product->id;
        if (Cache::has($key)) {
            return view('onekbuy.product.product', compact('product', 'productRelated', 'productPropose'));
            // $detail->view = Cache::get('detail');
        } else {
            Cache::put($key, 1, $minutes);
            $product->view +=1;
            $product->save();
        }
   
        return view('onekbuy.product.product', compact('product', 'productRelated', 'productPropose','profileId'));
    }

    public function searchProduct(Request $request){
        // dd($request->name);
        $search = Product::where('name','LIKE','%'.$request->name.'%')->orderby('id','desc')->paginate(18);
        // dd($search);
        // $search -> name = $request -> name;
        return view('onekbuy.product.search',compact('search'));
    }

    public function popularProduct(){
        $popularProductCategory = Product::where('active', 1)->orderBy('view', 'desc')->paginate(18);
        return view('onekbuy.product.popularproduct',compact('popularProductCategory'));
    }
    public function mostChosen(){
        $mostChosen = Product::where('active', 1)->orderBy('buyer_number', 'desc')->paginate(18);
        return view('onekbuy.product.mostchosen',compact('mostChosen'));
    }
    public function productpropose(){
        $productPropose = Product::inRandomOrder()->paginate(18);
        return view('onekbuy.product.productpropose',compact('productPropose'));
    }

    public function sort($sort){
        $countsort = Product::where('active', 1)->orderBy('view', 'desc')->get();

        if($sort == 'popularity'){
            $sorts = Product::where('active', 1)->orderBy('view', 'desc')->paginate(18);
        }
        elseif($sort == 'lastest'){
            $sorts = Product::where('active', 1)->orderBy('id','asc')->paginate(18);
        }
        elseif($sort == 'low-to-high'){
            $sorts = Product::where('active', 1)->orderBy('price','asc')->paginate(18);
        }
        elseif($sort == 'high-to-low'){
            $sorts = Product::where('active', 1)->orderBy('price','desc')->paginate(18);
        }
        elseif($sort == 'default'){
            $sorts = Product::where('active', 1)->orderBy('id','desc')->paginate(18);
        }
        else{
            return view('errors.404');
        }
        return view('onekbuy.product.sortbypopularity',compact('sorts','sort','countsort'));
    }

    public function order($id){

        $product = Product::where('active', 1)->with('user')->find($id);
        $profile = Auth::guard('web')->user()->profile;

        if($profile->phone_number == null || $profile->address == null){
            return redirect()->route('onekbuy.user.info')->with('error','Bạn cần nhập đầy đủ thông tin');
        }

        if(!$product){
            return redirect()->route('onekbuy.index.index')->with('error', 'Sản phẩm không tồn tại');
        }

        if(Auth::guard('web')->user()->wallet->credit_total <= $product->promotion_price){
            return redirect()->back()->with('error', 'Tài khoản của bạn không đủ tiền');
        }

        foreach(User::find(Auth::guard('web')->user()->id)->product as $item){
            if($item->id == $id){
                return redirect()->route('onekbuy.product.product', ['slug' => $product->slug, 'id' => $id])->with('error','Bạn đã đặt lệnh món đồ này');
            }
        }

        $userproduct = new Product_User();
        $userproduct->user_id = Auth::guard('web')->user()->id;
        $userproduct->product_id = $id;
        $userproduct->save();

        $wallet = Wallet::where('user_id', Auth::guard('web')->user()->id)->first();
        $wallet->credit_total -= $product->promotion_price;
        $wallet->save();

        $product->buyer_number += 1;
        $product->save();

        return redirect()->route('onekbuy.product.product', ['slug' => $product->slug, 'id' => $id])->with('success', 'Đặt lệnh thành công');
    }
}
