<?php

use Illuminate\Support\Facades\Route;


Route::group(['namespace' => 'Auth'], function () {
    Route::get('login', "AdminController@login")->name('auth.admin.login');
    Route::post('login', "AdminController@postLogin")->name('auth.admin.login');
    Route::get('logout', "AdminController@logout")->name('auth.admin.logout');
});


Route::group([ 'namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'admin'], function () {
    Route::get('/', 'AdminController@index')->name('admin.index');

    Route::group(['prefix' => 'users'], function () {
        Route::get('', 'UserController@index')->name('admin.users.index');
        Route::get('create', 'UserController@create')->name('admin.users.create');
        Route::post('create', 'UserController@store')->name('admin.users.store');
        Route::get('edit/{id}', 'UserController@edit')->name('admin.users.edit');
        Route::post('edit/{id}', 'UserController@update')->name('admin.users.update');
        Route::get('destroy/{id}', 'UserController@destroy')->name('admin.users.destroy');

        Route::group(['prefix' => 'profile'], function () {
            Route::get('{id}', 'ProfileController@index')->name('admin.profile.index');
            Route::post('/update/{id}', 'ProfileController@update')->name('admin.profile.update');
        });

        Route::group(['prefix' => 'wallet'], function () {
            Route::get('{id}', 'WalletController@index')->name('admin.wallet.index');
        });

        Route::group(['prefix' => 'refund'], function () {
            Route::get('', 'RefundController@index')->name('admin.refund.index');
            Route::get('delete/{id}', 'RefundController@destroy')->name('admin.refund.destroy');
            Route::get('active', 'RefundController@active')->name('admin.refund.active');
        });
    });

    Route::group(['prefix' => 'product'], function () {
        Route::get('activePost', 'ProductController@activePost')->name('admin.product.activePost');
        Route::get('detail/{id}', 'ProductController@detail')->name('admin.product.detail');
        Route::get('active', 'ProductController@active')->name('admin.product.active');
        Route::get('export', 'ProductController@export')->name('admin.product.export');
        Route::get('importExportView', 'ProductController@importExportView')->name('admin.product.viewimport');
        Route::post('import', 'ProductController@import')->name('admin.product.import');
        Route::get('history', 'ProductController@history')->name('admin.product.history');
    });
    Route::resource('product', 'ProductController', ['as' => 'admin']);
    Route::resource('category', 'CategoryController', ['as' => 'admin']);
    Route::resource('post', 'PostController', ['as' => 'admin']);

    Route::group(['prefix' => 'post'], function () {
        Route::get('active', 'PostController@active')->name('admin.post.active');
    });

    Route::resource('depositrequest', 'DepositRequestController', ['as' => 'admin'])->only([
        'index', 'destroy'
    ]);
    Route::get('active', 'DepositRequestController@active')->name('admin.depositrequest.active');

    Route::resource('notification', 'NotificationController', ['as' => 'admin']);
    Route::resource('informations', 'InforController', ['as' => 'admin']);



});

